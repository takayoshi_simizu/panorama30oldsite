<?php
require_once $classes_dir.'class.NavigateAdmin.php';
class NewsAdmin extends News {     function NewsAdmin($module, $moduleName){    	$this->News($module, $moduleName);
        $this->objDb = new DatabaseAdmin();
        $this->objTpl = new TemplatesAdmin();
        $this->objConf = new ConfigAdmin();

       	$this->page = 1;
		$this->per_page = 20;
	 	$this->from = 0;
	 	$this->count = $this->setCount();
	}
   	function drawThis() {
		if(isset($this->objConf->arr['add'])) {            $sql = $this->objDb->query("SELECT `ID` FROM `$this->table` ORDER BY `ID` DESC LIMIT 1");
			$id = $this->objDb->result($sql,0)+1;
			$this->objDb->query("INSERT `$this->table` VALUES('$id','��� ��������','','', '','".date("Y-m-d")."','','$this->language')");
			header("location: ".$this->objConf->globals['http_root'].$this->module);
		}
		elseif(isset($this->objConf->arr['delete']) && $this->newsID) {
		  	$this->objDb->query("DELETE FROM `$this->table` WHERE `ID` = '$this->newsID' LIMIT 1");
			header("location: ".$objConf->confArr['http_root'].$this->module);
		}
		elseif(isset($this->objConf->arr['edit']) && isset($_POST['id']) && isset($_POST['title']) and isset($_POST['stext']) and isset($_POST['ftext']) and isset($_POST['date']) and isset($_POST['type']) and isset($_POST['fpage'])) {
            $title = strtoupper(substr($_POST['title'],0,1)).strtolower(substr($_POST['title'],1,strlen($_POST['title'])));
			$d = explode('.',$_POST['date']);
		    $date = $d[2].'-'.$d[1].'-'.$d[0];
			$this->objDb->query("UPDATE `$this->table` SET `TITLE`='$title', `STEXT`='".$_POST['stext']."',`FTEXT`='".$_POST['ftext']."', `TYPE`='".$_POST['type']."', `FPAGE`='".$_POST['fpage']."',`DATE`='$date' WHERE `ID` = '".$_POST['id']."'");
		    header("location: ".$this->objConf->globals['http_root'].$this->module.'/id'.$_POST['id']);
		}
		else {
	        if($this->newsID) {	        	if($this->setProperties($this->newsID)) {
		        	$this->objTpl->setFile(1,'news-full.tpl');
			  		$this->objTpl->setVars(1,array('MODULE','ID','TITLE','STEXT','FTEXT','FPAGE','DATE', 'TYPE'));
			    	$this->objTpl->setValues(1,array($this->module,$this->newsID,$this->title,$this->objConf->wys_ready($this->stext),$this->objConf->wys_ready($this->ftext),$this->fpage, str_replace('-','.',$this->date), $this->type));
			    	$this->renderArray['content'] = $this->objTpl->getResult(1);
			    	$this->renderArray['title'] = $this->title;
		    	}
		    	else {
		    		$this->error = '����� ������� �� ����������!';
		    		return false;
		    	}
	        }
	        else {
	        	$this->objNav = new NavigateAdmin($this->page,$this->per_page,$this->count, $this->objConf->globals['http_root'].$this->module );
			    $this->page = $this->objNav->page;
			    $this->from = $this->objNav->from;

	        	$sql = $this->objDb->query("select * from `$this->table` WHERE `LANG`='$this->language' ORDER BY `DATE` DESC limit $this->from,$this->per_page");
	            for ($i=0;$i<$this->objDb->num_rows($sql);$i++)
	            	$this->renderArray['content'] .= $this->drawNewsElement($this->objDb->result($sql,$i),$i+1);

		       	if(empty($this->renderArray['content']))
		        	$this->renderArray['content'] = '<div class="sectionEpmty">'.$this->objConf->lngPack[$this->language]["ent_empty"].'</div>';

		       	$this->objTpl->setFile(1,'news.tpl');
		  		$this->objTpl->setVars(1,array('MODULE', 'NAVIGATE','ELEMENTS', 'COUNT'));
		    	$this->objTpl->setValues(1,array($this->module, $this->objNav->drawThis() ,$this->renderArray['content'],$this->count));
		    	$this->renderArray['content'] = $this->objTpl->getResult(1);
		    	$this->renderArray['title'] = '�������';
	        }
	        $this->objTpl->unsetFile(1);
	        $this->renderArray['module_name'] = $this->moduleName;
        }
    }

    function drawNewsElement($id, $num) {
    	$this->setProperties($id);
     	$this->objTpl->setFile(1,'news-elem.tpl');
	  	$this->objTpl->setVars(1,array('MODULE','ID','TITLE','STEXT','FTEXT','DATE', 'NUM'));
	   	$this->objTpl->setValues(1,array($this->module, $this->id,$this->title,$this->stext,$this->ftext, $this->objData->renderDate($this->date, '-'),$num));
	   	$element = $this->objTpl->getResult(1);
	   	$this->objTpl->unsetFile(1);
	   	return $element;
    }
}
?>
