<?php

class NavigateAdmin {

	# ����
    var $page = 1;          # ������� ��������
    var $per_page = 10;    	# ������� �� ��������
    var $count = 0;         # ����� �������

    # ��������
    var $pages_count;       # ���-�� �������
    var $from;              # ��������� ������
    var $ppage;             # ���������� ��������
    var $npage;             # ��������� ��������

    var $objTpl;

    function NavigateAdmin($page,$per_page,$count, $path){
        $this->objTpl = new Templates();
        $this->page = $page;
        $this->per_page = $per_page;
        $this->path = $path;
        $this->count = $count;
        $this->from = 0;
        $this->pages_count = 0;
        $this->ppage = 0;
        $this->npage = 0;
        $this->calculate();
    }

    function setPage($page){
        $this->page = $page;
    }

    function setPerPage($per_page){
        $this->per_page = $per_page;
    }

    function setCount($count){
        $this->count = $count;
    }

    function getPagesCount(){
        return $this->pages_count;
    }

    function getFrom(){
        return $this->from;
    }

    function getPage(){
        return $this->page;
    }

    function getPPage(){
        return $this->ppage;
    }

    function getNPage(){
        return $this->npage;
    }

    function calculate(){
        $this->pages_count = ceil($this->count/$this->per_page);
	    if ($this->pages_count == 0)
        	$this->pages_count = 1;
        $this->page = $this->check_page($this->page);
        $this->ppage = $this->check_page($this->page-1);
        $this->npage = $this->check_page($this->page+1);
        $this->from = ($this->page-1)*$this->per_page;
    }

    function check_page($page){
        if ($page <= 0)
        	$page = 1;
        if ($page > $this->pages_count)
        	$page = $this->pages_count;
        return $page;
    }

    function drawThis(){
        $content = '';
        if ($this->pages_count>1)
			$content = $this->drawThisList($this->drawThisElements());
        return $content;

    }

	function drawThisList($elements){
 		$this->objTpl->setFile(1,'navigate.tpl');
 		$this->to = ($this->from+$this->per_page<$this->count) ? $this->from+$this->per_page:$this->count;
  		$this->objTpl->setVars(1,array('ELEMENTS', 'COUNT', 'FROM', 'TO'));
        $this->objTpl->setValues(1,array($elements, $this->count, $this->from+1, $this->to));
        $content = $this->objTpl->getResult(1);
        $this->objTpl->unsetFile(1);
        return $content;
	}

	function drawThisElements(){
 		$content = '';
 		for ($i=1;$i<=$this->pages_count;$i++){
        	if ($i==$this->page)
        		$this->objTpl->setFile(1,'navigate-list-elem-n.tpl');
        	else
        		$this->objTpl->setFile(1,'navigate-list-elem-a.tpl');
  			$this->objTpl->setVars(1,array('HREF','NUM'));
     		$this->objTpl->setValues(1,array($this->path,$i));
        	$content .= $this->objTpl->getResult(1);
        }
        $this->objTpl->unsetFile(1);
        return $content;
	}

}

?>