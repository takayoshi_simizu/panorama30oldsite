<?php

////////////////////////////////////////////////////////////////////////////////
// ����� ��� ������ � ���������
////////////////////////////////////////////////////////////////////////////////

class Templates{

    var $files;				// ������ ��������
    var $vars;              // ����������
    var $values;            // ��������
    var $results;           // ����������
    var $opening_escape;    // ������ �����������
    var $closing_escape;   	// ����� �����������
    var $templates_path;   	// ���� � ��������

    function Templates($templates_path = ''){
        $this->files = array();
        $this->vars = array();
        $this->values = array();
        $this->results = array();
        $this->opening_escape = '{';
        $this->closing_escape = '}';
        $this->templates_path = './templates/';
    }

    function setFile($file_id,$file_name){
    	$file_content = join('',file($this->templates_path.$file_name)) or die("Couldn't open $file_name!");
        $this->files[$file_id] = $file_content;
        $this->results[$file_id] = '';
    }
    function setContent($file_id,$content){
        $this->files[$file_id] = $content;
        $this->results[$file_id] = '';
    }

    function setVars($file_id,$vars_array){
    	$this->vars[$file_id] = $vars_array;
    }

    function setValues($file_id,$values_array){
    	$this->values[$file_id] = $values_array;
    }

    function setTemplatesPath($templates_path){
    	$this->templates_path = $templates_path;
    }

    function replace($file_id){
    	$this->results[$file_id] = $this->files[$file_id];
        if (isset($this->vars[$file_id])){
        	for ($i=0;$i<sizeof($this->vars[$file_id]);$i++)
            	$this->results[$file_id] = str_replace($this->opening_escape.$this->vars[$file_id][$i].$this->closing_escape,$this->values[$file_id][$i],$this->results[$file_id]);
    	}
    }

    function getResult($file_id){
    	$this->replace($file_id);
        return $this->results[$file_id];
    }

    function resetFile($file_id){
    	$this->files[$file_id] = $this->getResult($file_id);
    }

    function unsetFile($file_id){
    	if (isset($this->files[$file_id]))
    		unset($this->files[$file_id]);
    	if (isset($this->vars[$file_id]))
    		unset($this->vars[$file_id]);
    	if (isset($this->values[$file_id]))
    		unset($this->values[$file_id]);
    	if (isset($this->results[$file_id]))
    		unset($this->results[$file_id]);
    }

}

?>