var page_;
var isEdit;
var sDate;
var eDate;
var perPage = 25;
var enabledModalDialog = true;
var defaultAction;
$(document).ready(function() {
    if(page)  {
 		 loadPage(page);
	}
	hightlightOdd()
	$('div.nav').click(function(){
        page = $(this).attr('page') || 1;
        loadPage(page);
		return false;
	});
	$('#sdate').datePicker(
		{
		horizontalPosition	: $.dpConst.POS_RIGHT,
		clickInput: true,
		createButton:false,
		startDate: '01-01-2005'
		}).bind(
				'dpClosed',
			function(e, selectedDates)
			{
                                    var d = selectedDates[0];
				if (d) {
					d = new Date(d);
					$('#edate').attr("value",d.asString()).dpSetStartDate(d.asString()).dpSetSelected(d.asString());
				}

			}
		)
	$('#edate').datePicker(
		{
		horizontalPosition	: $.dpConst.POS_RIGHT,
		clickInput: true,
		createButton:false,
		startDate: '01-01-2005'
	})
	$('#sdate_').datePicker(
		{
		horizontalPosition	: $.dpConst.POS_RIGHT,
		clickInput: true,
		createButton:false,
		startDate: '01-01-2005'
		}).bind(
				'dpClosed',
			function(e, selectedDates)
			{
   				var d = selectedDates[0];
				if (d) {
					d = new Date(d);
					$('#edate_').attr("value",d.asString()).dpSetStartDate(d.asString()).dpSetSelected(d.asString());
				}

			}
		)
	$('#edate_').datePicker(
		{
		horizontalPosition	: $.dpConst.POS_RIGHT,
		clickInput: true,
		createButton:false,
		startDate: '01-01-2005'
	})
	windowAnimation();
});


function showLoader() {
	$("#loader").html("&nbsp;&nbsp;<img src='"+imgLoader.src+"' />");//add loader to the page
}

function hightlightOdd() {
    $("tr.edit_elem").css({height: 50});
    $('#content').find("tr.edit_elem:nth-child(odd)").animate({backgroundColor: '#D8FAFF'}, 500).attr('back', '#D8FAFF');
}

function loadPage(page) {
	$.post(phppage, {page: page}, onSuccess);
	$('#content').css({opacity: 0.5});
}
function onSuccess(data) {
	$('#content').html(data);
	$('#loader').empty();
	isEdit = false;
        $('#content').css({opacity: 1});
	// NAVIGATION -----------------------------------
	$('div[@page='+page_+']').removeClass('nav_cur');
        $('div[@page='+page+']').addClass('nav_cur');
        page_ = page;
       //-----------------------------------------------
    hightlightOdd();
}


function edit(ID) {
    editedElem =   $('#'+ID);
    if(!isEdit) {
	    isEdit = editedElem;

	    type = $(this).attr('type') || false;
	 	name = $(this).attr('name') || false;


	    $('#content').find('tr.edit_elem').each(function() {
	    	if( this.id!= ID)
	     		$(this).css({opacity: 0.2})
	    })
	    editedElem.animate({backgroundColor: '#CFFDD1', height: 150},500).find('td').each(function(){
name= $(this).attr('name');
type= $(this).attr('type');
			old_ = ($(this).html() == '') ? ' ' : $(this).html();
			$(this).attr('old', old_);


	 		if(type == 'textarea') {
	 			$(this).html('<textarea class="elem" id="text_'+name+'" name="'+name+'">'+ old_.replace(/<br>/gi, '\n')+'</textarea>');
	 			//$('#text_'+name).Autoexpand(200);
	 		}
	 		else if(type == 'text') {
	 			$(this).html('<input class="elem" name="'+name+'" value="'+old_+'" />');
	 		}
	 		else if(type == 'enum') {
	 			array_enum = eval(name+'_enum')
	 			var select = '<select type="enum" id="'+name+'" name="'+name+'" class="elem">';
	 			for(var i=0; i< array_enum.length; i++) {
	 				selected = (old_==array_enum[i])? 'selected' : '';
	 				select += '<option '+selected+'>'+array_enum[i]+'</option>';
	 			}
	 			select += '</select>'
	 			$(this).html(select);
	 		}
	 		else if(type == 'date') {
	 				$(this).html('<input class="elemDate" name="'+name+'" value="'+old_+'" />');
	 		       $('.elemDate').datePicker({clickInput: true,createButton:false,startDate: '01-01-2005'});
	 		}
	 		else if(type == 'dateInstall') {
	 		     img=''
	 		     // img = (enabledModalDialog) ? '<div style="float:right"><a class="thickbox" href="install.php?KeepThis=true&TB_iframe=true&height=500&width=750"><img src="./design/calendar.png"></a></div>' : ''
	 				$(this).html(img + '<input id="'+name+'" class="elemDateInstall" name="'+name+'" value="'+old_+'" />');
	 		       $('.elemDateInstall').datePicker({
	 		       		clickInput: true,
						createButton:false
	 		       	})
	 		}
	 		else if(type == 'dateCalc') {
	 		    old_date = $(this).find('#date_').html();
	 		    old_time = $(this).find('#time_'+ID).html();
	 	 		img='' //img = (enabledModalDialog) ? '<div style="float:right"><a class="thickbox" href="calculate.php?KeepThis=true&TB_iframe=true&height=500&width=750"><img src="./design/calendar.png"></a></div>' : ''
                //times = old_time.split("|")
		 		$(this).html(img + '<div id="date_"><input id="dateCalc" id="'+name+'" class="elemDate" name="'+name+'" value="'+old_date+'" /></div><div id="time_'+ID+'"><div id="timeCalcCont'+ID+'"></div></div>');
		 		$('input.elemDate').datePicker({
					startDate: '01-01-2000', clickInput: true,createButton:false
				}).bind('dateSelected', function(e, selectedDate, $td, state) {
			 		getTime(ID)
				});

			}
		});
		defaultAction = $('#actions'+ID).html()
		$('#actions'+ID).html("<div class='dialog_text'>�������������?</div><img class='action' src='./design/save.png' onclick='save("+ID+",\"edit\",true)'> &nbsp; <img class='action' src='./design/false.png' onclick='save("+ID+",\"edit\",false)'>")
        if(enabledModalDialog==true)
        	tb_init('a.thickbox, area.thickbox, input.thickbox');
	}

	if(calculation) {
		$(editedElem).find('#spec').change(function(){getTime(ID)})
 		getTime(ID)
 	}
}

function remove(ID)
{
	elem =   $('#'+ID).get();
	if(!isEdit) {
        isEdit = elem;
	    // ������������� ���� ��������� ������������  - ����� ��������
	   	$('#content').find('tr.edit_elem').each(function()
	   	{
		   if( this.id!=ID ){ $(this).animate({opacity: 1}, 500) }
		});
	    $('#'+ID).animate({backgroundColor: '#FEF3D3', height: 150},500);
		defaultAction = $('#actions'+ID).html()
		$('#actions'+ID).html("<div class='dialog_text'>�������?</div><img  class='action' src='./design/true.png' onclick='save("+ID+",\"delete\",true)'> &nbsp; <img  class='action' src='./design/false.png' onclick='save("+ID+",\"delete\",false)'>")
	}
}
function save(ID, action ,confirm)
{
	if(confirm) //-----------------------------------�������������
	{
		if (action=='edit') {
		eval("$.post('db.php', {action: 'edit', " + getLine(ID) + "}, onDbUpdate)")

		}
		else if(action=='delete')  {
			$('#'+ID).css({ backgroundColor:"#E7D1FF", opacity: 0.4})
 			eval("$.post('db.php', {action: 'delete', id: '" + ID + "'}, onDbUpdate)")
		}
	}
	else {  // ----------------------------------------------������
		if(action =='edit')
			$('#'+ID).find('td').each(function(){ $(this).empty().html($(this).attr('old'))	});
	}
	// ������� ���� ��������� ������������  - ����� ��������
   	$('#content').find('tr.edit_elem').each(function() 	{  if( this.id!=ID ){ $(this).css({opacity: 1}) }});
	// ��������� �������� ����
	if (action=='delete' && confirm)
	{
		$('#'+ID).animate({height: 40},500)
		$('#actions'+ID).html("<a onclick='restore("+ID+")'>������������</a>").css({opacity: 1})
	}
	else
	{
        var back = $('#'+ID).attr('back')
		$('#'+ID).animate({backgroundColor: back, height: 70},500);
		$('#actions'+ID).html(defaultAction)
	}
    isEdit = false;
}

function  restore(ID)
{
	eval("$.post('db.php', {action: 'add', " + getLine(ID) + "}, onDbUpdate)")
	var back = $('#'+ID).attr('back')
	$('#'+ID).animate({backgroundColor: back, height: 70, opacity: 1},500);
	$('#actions'+ID).html($('#actions'+ID).attr('default'))
    return false;
}
function getLine(ID)
{
        var params = '';
		$('#'+ID).find('td').find('input[@type],textarea[@type],select[@type]').each(function() {
	        if(this.name!= 'print') {
		        if($(this).attr('type') == 'enum') {
		 			value = this.options[this.selectedIndex].text
		    	}
	            else {
					value = $(this).attr('value');
					if(value==undefined)
	    				value = '&nbsp;'
		   		}
	    	    value = value.replace(/\n/gi, "<br>")
		    	value = value.replace(/\s/gi, "&nbsp;")
		    	if(this.name=='time') {
		    		value = '';
        			$(this).children("option:selected").each(function(){
                   	    value += $(this).attr("id")+',';
           			})
              		value = value.replace(/,$/,"")
		    	}
                if(this.name) {
                	params += this.name + ": '" + value + "',"
                }
                // ���� �������������� ������ ������ ��������� ������������ ��������
	            if(parseInt(ID)) {
	            	if(this.name=='time') {
                        $('#timeCalcCont'+ID).html(value)
	            	}
	            	else
	            		$(this.parentNode).html(value);

	            }

            }
	});
	if(params == '') {  //��� ��������
		$('#'+ID).find('td[@rest]').each(function() {
			params += $(this).attr('name') + ": '" + $(this).html() + "',"
		});
	}

	return "id : '" + ID + "'," + params.replace(/,$/,"");
}
function print() {
	list = '';
	$('input[@name=print]:checked').each(function(){
			list += this.value +  '+'
	})
    $.get('print.php', {list: list}, onPrintSucces)
}

function printall(obj) {
	$('input[@name=print]').check('toggle');

}
function onPrintSucces(data) {
   document.open()
   document.write(data)
   document.close()
}
function addhide() {
	$('#new').css('display','none')
}
function add() {
	str = "action: 'add', " + getLine("new_");
	eval("$.post('db.php', {"+str+"}, onDbAdd)");
	showLoader();

}
function onDbUpdate(data) {
	//if(data.length>1)
	//	alert(data)
}
function onDbAdd(data) {
      window.location.reload()
}
function showByDate (page) {
	sDate = $('#sdate').attr("value");
   	eDate = $('#edate').attr("value");
   	showLoader()
   	$.post(page, {sdate: sDate, edate: eDate}, onSuccess);
}
function showByText (page) {
   	_search = $('#search').attr("value")
   	$('#search').attr("value","")
   	showLoader()
   	$.post(page, {search: _search}, onSuccess);
}


function getTime(ID) {
	  id = (ID) ? ID : false;
	  if(id) {
	      date = $('#'+id).find('#dateCalc').attr('value');
	      spec = $('#'+id).find('#spec').attr('value');
	  }
	  else {
	  	date = 	$('#dateCalc').attr('value');
	    spec = $('#spec').attr('value');
	  }
      if(date && spec) {
      	if(id)
      		$.post("calculatedate.php", {id:id, date: date, spec: spec}, onTimeChane_)
        else
        	$.post("calculatedate.php", {id:id, date: date, spec: spec}, onTimeChane)

      }
		function  onTimeChane(data) {
		    $('#timeCalcCont').html(data)
		    $("#times").asmSelect({
						addItemTarget: 'bottom',
						animate: true,
			});

		}
		function  onTimeChane_(data) {
		    $('#timeCalcCont'+id).html(data).find("#times").asmSelect({
						addItemTarget: 'bottom',
						animate: true,
			});
		}


}

//== OK++++++++++++++++++++++++++++
function downloadBase() {
	sDate = $('#sdate').attr("value");
   	eDate = $('#edate').attr("value");
   	base = $('input.01:checked').attr("value");
	url = 'export.php?sdate='+sDate+'&edate='+eDate+'&base='+base;
	window.location.href = url;
	location.replace(url)
	location = url;
}
function clearBase() {
	if(confirm('�� �������?')) {
		sDate = $('#sdate_').attr("value");
	   	eDate = $('#edate_').attr("value");
	   	$.post('clear.php', {sdate: sDate, edate: eDate}, onClearSuccess);
   	}
}
function onClearSuccess(data) {
 	alert(data)
}
function showOverlay() {
	if (typeof document.body.style.maxHeight === "undefined") {//if IE 6
			$("body","html").css({height: "100%", width: "100%"});
			$("html").css("overflow","hidden");
			if (document.getElementById("TB_HideSelect") === null) {//iframe to hide select elements in ie6
				$("body").append("<iframe id='TB_HideSelect'></iframe><div id='TB_overlay'></div><div id='TB_window'></div>");
				$("#TB_overlay").click(tb_remove);
			}
		}else{//all others
			if(document.getElementById("TB_overlay") === null){
				$("body").append("<div id='TB_overlay'></div><div id='TB_window'></div>");
				$("#TB_overlay").click(tb_remove);
			}
		}
 	       $("#TB_overlay").addClass("TB_overlayBG");
}
function tb_showIframe(){
	$("#TB_load").remove();
	$("#TB_window").css({display:"block"});
}

function tb_position() {
$("#TB_window").css({marginLeft: '-' + parseInt((TB_WIDTH / 2),10) + 'px', width: TB_WIDTH + 'px'});
	if ( !(jQuery.browser.msie && jQuery.browser.version < 7)) { // take away IE6
		$("#TB_window").css({marginTop: '-' + parseInt((TB_HEIGHT / 2),10) + 'px'});
	}
}

function tb_remove() {
 	$("#TB_imageOff").unbind("click");
	$("#TB_closeWindowButton").unbind("click");
	$("#TB_window").fadeOut("fast",function(){$('#TB_window,#TB_overlay,#TB_HideSelect').trigger("unload").unbind().remove();});
	$("#TB_load").remove();
	if (typeof document.body.style.maxHeight == "undefined") {//if IE 6
		$("body","html").css({height: "auto", width: "auto"});
		$("html").css("overflow","");
	}
	document.onkeydown = "";
	document.onkeyup = "";
	return false;
}


function windowAnimation() {
	$('#windowOpen').bind(
			'click',
			function() {
				if($('#window').css('display') == 'none') {
					$('#window').show();
					$('#new_').find('input,textarea').each(function() {
							if(this.id != 'u_id')
								$(this).attr("value", "")
					})
					$('#timeCalcCont').html('<table id="caution" cellpadding=0 cellspacing=0><tr valign=middle><td class="back"><img src="./design/caution.png" /></td><td width=150px class="back">�������� ���� � �����������!</td></tr></table>');



					$('.inputDate').datePicker({startDate: '01-01-2000', clickInput: true,createButton:false});
					$('.inputDateCalc').datePicker({
						startDate: '01-01-2000', clickInput: true,createButton:false
					}).bind('dateSelected', function(e, selectedDate, $td, state) {
						getTime();
					});
				}
				this.blur();
				return false;
			}
		);
		$('#windowClose').bind(
			'click',
			function()
			{
			$('#window').hide();
			}
		);
		$('#windowMin').bind(
			'click',
			function()
			{
				$('#windowContent').hide();
				$('#windowBottom, #windowBottomContent').animate({height: 10}, 300);
				$('#window').animate({height:40},300)
				$(this).hide();
				$('#windowResize').hide();
				$('#windowMax').show();
			}
		);
		$('#windowMax').bind(
			'click',
			function()
			{
				var windowSize = $.iUtil.getSize(document.getElementById('windowContent'));
				$('#windowContent').show();
				$('#windowBottom, #windowBottomContent').animate({height: windowSize.hb + 13}, 300);
				$('#window').animate({height:windowSize.hb+43}, 300)
				$(this).hide();
				$('#windowMin, #windowResize').show();
			}
		);
		$('#window').Resizable(
			{
				minWidth: 200,
				minHeight: 60,
				maxWidth: 700,
				maxHeight: 400,
				dragHandle: '#windowTop',
				handlers: {
					se: '#windowResize'
				},
				onResize : function(size, position) {
					$('#windowBottom, #windowBottomContent').css('height', size.height-33 + 'px');
					var windowContentEl = $('#windowContent').css('width', size.width - 25 + 'px');
					if (!document.getElementById('window').isMinimized) {
						windowContentEl.css('height', size.height - 48 + 'px');
					}
				}
			}
		);



}
jQuery.fn.check = function(mode) {
   var mode = mode || 'on';
   return this.each(function()
   {
     switch(mode) {
       case 'on':
         this.checked = true;
         break;
       case 'off':
         this.checked = false;
         break;
       case 'toggle':
         this.checked = !this.checked;
         break;
     }
   });
};


