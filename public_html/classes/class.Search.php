<?php

class Search {
	private $objNav;
	private $objDb;
	private $objTpl;
	private $objTsk;
	private $objConf;

    protected $id;
    protected $title;
	protected $stext;
	protected $type;
    protected $ftext;
    protected $fpage;
    protected $date;

    private $page = 1;
	private $per_page = 10;
	private $from = 0;
	private $count = 0;

    public function Search($search){        $this->objDb = new Database();
        $this->objTpl = new Templates();
        $this->objConf = new Config();
      	$this->objURL = new URL();
        $this->objMod = new Modules();

	 	$this->search = $search;
	 	$this->language = $this->objConf->setLanguage();

	 	$this->found ='';
	}
	protected function setProperties($id){
    	$sql = $this->objDb->query("select * from `$this->table` where `ID`='$id' and `LANG`='$this->language'  limit 1");
    	if($this->objDb->num_rows($sql)>0) {
	       	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
	       	$this->id = $id;
	       	$this->title = $row["TITLE"];
	       	$this->type = $row["TEXT"] || $row["STEXT"];
	        return 1;
        }
        return 0;
    }

   	public function drawThis() {
   	    $printArray = array();
        // CONTENT  ************************************************************
        $this->printArray['page_content'] = "<div style='margin-top: 15px'>�� ������: <b>$this->search</b></div>";

        $sql = $this->objDb->query("select * from `".$this->objMod->table."` WHERE `LANG`='$this->language' ORDER BY `ID` DESC");
	   	for ($i=0;$i<$this->objDb->num_rows($sql);$i++) {	   		$row_mod = $this->objDb->aoutput($this->objDb->fetch_array($sql));
	   		if($row_mod["MODULE"]!='Menu') {
	            $string = "new ".$row_mod["MODULE"]." ('".$row_mod["PATH"]."','".$row_mod["TITLE"]."');";
		   		eval('$this->objPrt = '.$string);
		   		if($row_mod["MODULE"]=='StaticPage') {
		   			$sql_search = $this->objDb->query("select * from `".$this->objPrt->table."` WHERE (`TITLE` LIKE '%$this->search%' or `TEXT` LIKE '%$this->search%') AND  `LANG`='$this->language' ORDER BY `ID` DESC");
                    if($this->objDb->num_rows($sql_search)>0) {                    	$this->found .= '<div id="searchModule"><div id="searchModuleName">'.$row_mod["TITLE"].'</div>';
	                    for ($j=0;$j<$this->objDb->num_rows($sql_search);$j++) {	                    	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql_search));
	                    	 if(strstr($row['TITLE'],$this->search))
	                    	 	$match = $row['TITLE'];
	                    	 else
	                    		$match= $row['TEXT'];
                            $this->found .= $this->drawElement($row_mod['PATH'].'/'.$this->objPrt->getTree($row['ID']), $row['TITLE'],$this->getPart($match));
	                    }
	                    $this->found .= '</div>';
                    }


		   		}
		   		elseif($row_mod["MODULE"]=='Gallery') {		   			$sql_search = $this->objDb->query("select * from `".$this->objPrt->album_table."` WHERE (`TITLE` LIKE '%$this->search%' or `TEXT` LIKE '%$this->search%') AND  `LANG`='$this->language' ORDER BY `ID` DESC");
		   		    if($this->objDb->num_rows($sql_search)>0) {
                    	$this->found .= '<div id="searchModule"><div id="searchModuleName">'.$row_mod["TITLE"].'</div>';
	                    for ($j=0;$j<$this->objDb->num_rows($sql_search);$j++) {
	                    	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql_search));
                            if(strstr($row['TITLE'],$this->search))
	                    	 	$match = $row['TITLE'];
	                    	else
	                    		$match= $row['TEXT'];

                            $this->found .= $this->drawElement($row_mod['PATH'].'/album'.$row['ID'], $row['TITLE'],$this->getPart($match));

	                    }
	                    $this->found .= '</div>';
                    }
		   		}
		   		elseif($row_mod["MODULE"]=='News') {		   			$sql_search = $this->objDb->query("select * from `".$this->objPrt->table."` WHERE (`TITLE` LIKE '%$this->search%' or `STEXT` LIKE '%$this->search%' or `FTEXT` LIKE '%$this->search%') AND  `LANG`='$this->language' ORDER BY `ID` DESC");
		   		     if($this->objDb->num_rows($sql_search)>0) {
                    	$this->found .= '<div id="searchModule"><div id="searchModuleName">'.$row_mod["TITLE"].'</div>';
	                    for ($j=0;$j<$this->objDb->num_rows($sql_search);$j++) {
	                    	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql_search));
                             if(strstr($row['TITLE'],$this->search))
	                    	 	$match = $row['TITLE'];
	                    	elseif(strstr($row['STEXT'],$this->search))
	                    	   $match = $row['STEXT'];
	                    	else
	                    		$match= $row['FTEXT'];

                            $this->found .= $this->drawElement($row_mod['PATH'].'/album'.$row['ID'], $row['TITLE'],$this->getPart($match));
	                    }
	                    $this->found .= '</div>';
                    }
		   		}
	   		}
	   	}
	   	if(empty($this->found))
	   			$this->found = '<div style="margin-top: 25px;margin-bottom: 80px;">������ �� �������!</div>';
	   	$this->printArray['page_content'] .= $this->found;
	   	$this->printArray['page_title'] = '�����';
        $this->printArray['module_name'] = '�����';
        return $this->printArray;
    }

   private function drawElement ($href, $title, $text) {   		$this->objTpl->setFile(1,'search-elem.tpl');
	  	$this->objTpl->setVars(1,array('HREF','SEARCH','TITLE','TEXT'));
	   	$this->objTpl->setValues(1,array($href,$this->search, $title, $text));
	   	$element = $this->objTpl->getResult(1);
	   	$this->objTpl->unsetFile(1);
        return $element;
   }

   private function getPart ($text) {   	    $text = strip_tags($text);

   	    $text = eregi_replace($this->search, '<span style="background: #F1FF52;  color: black">'.$this->search.'</span>', $text);
   		return $text;
   }
}

?>
