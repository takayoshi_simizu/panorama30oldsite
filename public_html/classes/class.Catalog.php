<?php
require_once $classes_dir.'class.Img.php';
require_once $classes_dir.'class.Navigate.php';
class Catalog {
    function Catalog($module, $moduleName){
        $this->objDb = new Database();
        $this->objTpl = new Templates();
        $this->objConf = new Config();
        $this->objIMG = new IMG();
		$this->objFileSystem = new FileSystem();

        $this->module = $module;
	 	$this->moduleName = $moduleName;
        $this->album_table =  $this->module.'_categoris';
        $this->item_table =  $this->module.'_items';

        $this->cat_id = (isset($this->objConf->arr['category'])) ?  $this->objConf->arr['category'] : 0;
        $this->item_id = (isset($this->objConf->arr['item'])) ?  $this->objConf->arr['item'] : 0;
        $this->page = (isset($this->objConf->arr['page'])) ?  $this->objConf->arr['page'] : 1;
        $this->id = (isset($this->objConf->arr['id'])) ? $this->objConf->arr['id'] : 0;

        $this->renderArray['title'] =  $this->moduleName;
        $this->language = $this->objConf->setLanguage();

        $this->objFileSystem->makedir($this->objConf->globals['root'].'/content/'.$this->module);
        $this->objFileSystem->makedir($this->objConf->globals['root'].'/content/'.$this->module.'/text');
        $this->objFileSystem->makedir($this->objConf->globals['root'].'/content/'.$this->module.'/ftext');

        $this->setCount();
	}

   	function setCount(){
     	$sql = $this->objDb->query("select count(*) from `$this->item_table`");
     	$this->count = $this->objDb->result($sql,0);
    }
	function getParentTitle ($id)  {
		if(!$id)
			return '<a href="{HTTP_ROOT}'.$this->module.'">�������</a> / ';
        $sql = $this->objDb->query("select * from `$this->album_table` where `ID`='$id'  limit 1");
    	if($this->objDb->num_rows($sql)) {
	    	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
	    	$path='<a href="{HTTP_ROOT}'.$this->module.'/category'.$row["ID"].'">'.$row["TITLE"].'</a> / ';
	     	$path =  $this->getParentTitle($row["PAR_ID"]).$path;
	    	return $path;
    	}
	}
	function drawThis () {
       if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
   			if(isset($_POST['id'])&& isset($_POST['sum']) && isset($_POST['act']) && $_POST['act']=='vote') {
   				$this->id = (int) $_POST['id'];
		   		$this->sum =(int) $_POST['sum'];
		   		$this->voted[$this->id] = 'true';
   				$this->text = '������� �� �����!';
   				if(!isset($_COOKIE['item'.$this->id])) {
		      		setcookie('item'.$this->id,'true', time()+10*365*24*3600,'/');
		        	$sql = $this->objDb->query("select * from `$this->item_table` where `ID`='$this->id'");
		         	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
		         	$this->num =  (int) $row['NUM'] + 1;
		         	$this->sum =  (int) $row['SUM'] + $this->sum;
		          	$this->objDb->query("update `$this->item_table` set `NUM` = '$this->num',`SUM` = '$this->sum' where `ID`='$this->id'");
       		 	}
       		 	else $this->text = '�� ��� ����������!';

          		$this->rating  = ($this->num!=0) ? round(($this->sum/$this->num),1) : "<span style='font-size: 11px; color: #92A7B8'>������� ������� �� ��������, ��� ��� ���������� ������� ����� ����</span>";

                $sql = $this->objDb->query("select * from `$this->item_table` where `ID`='$this->id'");
		       	if($this->objDb->num_rows($sql)!=0) {
		       		$this->objTpl->setFile(1,'rating_vote_.tpl');
					$this->objTpl->setVars(1,array('MODULE','NUM','SUM','RATING', 'TEXT','NUM_TEXT'));
			    	$this->objTpl->setValues(1,array($this->module,$this->num,$this->sum,$this->rating,$this->text,$this->nummmed($this->num)));
		      		echo $this->objTpl->getResult(1);
		      		$this->renderedPage = $this->objTpl->getResult(1);
		      		return true;
		       	}
		       	else
		       		echo '������!';
   			}
   			return true;
       } else {
       if($this->item_id) {
         	$sql = $this->objDb->query("select * from `$this->item_table` WHERE `ID`='$this->item_id' limit 1");

          	if(!$this->objDb->num_rows($sql)) {
          		$this->error = '����� ������������ �� ����������!';
          		return false;
          	}


          	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
          	$this->title =  $row['TITLE'];
          	$this->a_id = $row['PAR_ID'];
          	//$this->num = (int) $row['NUM'];
          	//$this->sum = (int) $row['SUM'];
          	//$this->rating  = ($this->num!=0) ?  round(($this->sum/$this->num),1) : "<span style='font-size: 11px; color: #92A7B8'>������� ������� �� ��������, ��� ��� ���������� ������� ����� ����</span>";

            if(!isset($this->voted['item'.$this->item_id]))
				$this->voted = (isset($_COOKIE['item'.$this->item_id])) ? true : false;

           	if($this->voted)
        		$this->objTpl->setFile(1,'rating_vote_.tpl');
    		else
    			$this->objTpl->setFile(1,'rating_vote.tpl');
			$this->objTpl->setVars(1,array('MODULE','ID','TEXT','NUM_TEXT'));
	    	$this->objTpl->setValues(1,array($this->module,$this->item_id,$this->num,$this->sum,$this->rating,'',$this->nummmed($this->num)));
      		$rating = $this->objTpl->getResult(1);
      		/* --------------------------------------------*/

        	$this->hierarchy = $this->getParentTitle($this->a_id).$this->title;

        	$this->objTpl->setFile(1,'item.tpl');
			$this->objTpl->setVars(1,array('MODULE','ID','TITLE','TEXT', 'FTEXT','RATING_VOTE','HIERARCHY'));
	    	$this->objTpl->setValues(1,array($this->module,$this->item_id,$this->title,$row["TEXT"],$row["FTEXT"],$rating,$this->hierarchy));
      		$this->renderArray['content'] = $this->objTpl->getResult(1);
      		return true;
        }
        else {
        	$this->drawCatAlbum($this->cat_id);
            if($this->cat_id) {
        		$sql = $this->objDb->query("select * from `$this->album_table` WHERE `ID`='$this->cat_id'  limit 1");
         		if($this->objDb->num_rows($sql)) {
      				$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
            		$this->cat_title = $row['TITLE'];
           		 	$this->cat_par_id = $row['PAR_ID'];
    				$this->hierarchy = ($this->cat_id)? $this->getParentTitle($this->cat_par_id).$this->cat_title : '';
            	}
            	else { 	$this->error = '����� ��������� �� ����������!';  }
            }
            $this->objTpl->setFile(1,'catalog.tpl');
		 	$this->objTpl->setVars(1,array('MODULE', 'ID','ELEMENTS_CATEGORY','ELEMENTS_PRODUCTS' ,'NAVIGATE', 'PAGE_TITLE','HIERARCHY'));
		  	$this->objTpl->setValues(1,array($this->module,$this->cat_id,$this->renderCat,$this->renderItm ,'', $this->renderArray['title'],$this->hierarchy));
        }
	    $this->renderArray['title'] = $this->moduleName;
	    $this->renderArray['content'] = $this->objTpl->getResult(1);
	 	$this->objTpl->unsetFile(1);
	 	}
	}
	function drawCatAlbum($id) {
  		$this->renderCat = '';
  		$this->renderItm = '';
    	$sql = $this->objDb->query("select `ID` from `$this->album_table` WHERE `PAR_ID`='$id' and `LANG` = '$this->language' order by `ID`");
	   	for ($i=0;$i<$this->objDb->num_rows($sql);$i++)
	    	$this->renderCat .= $this->drawCategoryElement($this->objDb->result($sql,$i),$i+1);
	    //if(empty($arr['elements_catagory']))
	   // 	$arr['elements_catagory'] = '<tr><td>����������� ���!</td></tr>';
        // DRAW PRODUCTS ****************************************************
       	$sql = $this->objDb->query("select `ID` from `$this->item_table`  WHERE `A_ID`='$id' and `LANG` = '$this->language' order by `ID` DESC");
	   	for ($i=0;$i<$this->objDb->num_rows($sql);$i++)
	    	$this->renderItm .= $this->drawProductElement($this->objDb->result($sql,$i),$i+1);

	   // if(empty($arr['elements_products']))
	   // 	$arr['elements_products'] = '<tr><td>������������ ���!</td></tr>';
    }
    function drawCategoryElement($id,$i){
 		$sql = $this->objDb->query("select * from `$this->album_table` where `ID`='$id'  limit 1");
       	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
   		$this->objTpl->setFile(1,'catalog-elem.tpl');
  		$this->objTpl->setVars(1,array('MODULE','ID','IMG','TITLE','DATE', 'NUM'));
    	$this->objTpl->setValues(1,array($this->module,$id,'',$row["TITLE"],$this->objDb->doutput($row["DATE"]), $i));
    	$content = $this->objTpl->getResult(1);
        $this->objTpl->unsetFile(1);
        return $content;
	}
	function drawProductElement($id,$i){
 		$content = '';
 		$sql = $this->objDb->query("select * from `$this->item_table` where `ID`='$id'  limit 1");
       	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
       	if(empty($row["TEXT"])) {
       		$row["TEXT"] = '��� ��������';
       	}
       	$this->path = 'content/'.$this->module.'/'.$row["ID"].'/';
        $this->img = $this->objIMG->get($this->path.'thumbs/'.$row["ID"].'.jpg',$this->objConf->globals['http_root'].$this->path.$row["ID"].'.jpg');

        if(!empty($row['FTEXT'])) {
        	$row['FTEXT'] = '<a class="more" href="{HTTP_ROOT}'.$this->module.'/item'.$row['ID'].'">[���������]</a>';
        }
   		$this->objTpl->setFile(1,'catalog-elem-product.tpl');
  		$this->objTpl->setVars(1,array('MODULE','ID','TITLE','TEXT','FTEXT', 'IMG','NUM'));
    	$this->objTpl->setValues(1,array($this->module,$id,$row["TITLE"],$row["TEXT"], $row['FTEXT'], $this->img,$i));
    	$content .= $this->objTpl->getResult(1);
        $this->objTpl->unsetFile(1);
        return $content;
	}

	function nummmed($num) {
		$letter = substr($num,(sizeof($num)-1),1);
		if($letter==0)
        	return '';
		elseif($letter==1) {
			return $num.' �����';
		}
        elseif($letter>=2 and $letter<=4)
        	return $num.' ������';
        else
        	return $num.' �������';
	}
}
?>
