<?php
class StaticPage {
    function StaticPage($module, $moduleName){    	$this->module = $module;
        $this->moduleName = $moduleName;

        $this->objDb = new Database();
        $this->objTpl = new Templates();
        $this->objConf = new Config();

        $this->id = 0;

       	$this->table = $this->module;
       	$this->language = $this->objConf->setLanguage();
		$this->level=0;
	}
	function setProperties($id){
    	$sql = $this->objDb->query("select * from `$this->table` where `ID`='$id'  limit 1");
       	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
       	$this->subpage_id = $row["ID"];
       	$this->title = $row["TITLE"];
       	$this->text = $row["TEXT"];
       	$this->par_id = $row["PAR_ID"];
       	$this->path = $row["PATH"];
       	$this->tree_path = $this->getTree($row["ID"]);
       	if($this->objDb->num_rows($this->objDb->query("SELECT * FROM `$this->table` WHERE `PAR_ID`='$this->subpage_id' "))>0)
       		$this->children = 'dir';
       	else
       		$this->children = 'file';
    }

    function drawSubpages($id) {    	$elements='';
   		$sql = $this->objDb->query("select `ID` from `$this->table` where `PAR_ID`='$id'  and `LANG`='$this->language'");
         for ($i=0;$i<$this->objDb->num_rows($sql);$i++)  {
	        $this->setProperties($this->objDb->result($sql,$i));
	        if($i==$this->objDb->num_rows($sql)-1)
	        	$this->objTpl->setFile(2,'sub-pages-elem-n.tpl');
	        else
	 	    	$this->objTpl->setFile(2,'sub-pages-elem.tpl');
	  		$this->objTpl->setVars(2,array('MODULE','ID','TITLE','TREE_PATH','PATH','NUM', 'TYPE'));
	    	$this->objTpl->setValues(2,array($this->module,$this->subpage_id,$this->title,$this->tree_path,$this->path, $i+1,$this->children ));
	    	$elements .= $this->objTpl->getResult(2);
	        $this->objTpl->unsetFile(2);
        }

       return $elements;
    }

    function drawTree($id) {
    	if($id!=0){
    		$elements='';
   			$sql = $this->objDb->query("select `ID` from `$this->table` where `PAR_ID`='$id'  and `LANG`='$this->language'");
	         for ($i=0;$i<$this->objDb->num_rows($sql);$i++)  {
		        $this->setProperties($this->objDb->result($sql,$i));
		        if($this->drawPageID==$this->subpage_id)
		        	$this->objTpl->setFile(1,'hierarchy-elem-cur.tpl');
		        else
		 	  		$this->objTpl->setFile(1,'hierarchy-elem.tpl');
		  		$this->objTpl->setVars(1,array('MODULE','ID','TITLE','TREE_PATH','PATH','NUM', 'TYPE','LEVEL'));
		    	$this->objTpl->setValues(1,array($this->module,$this->subpage_id,$this->title,$this->tree_path,$this->path, $i+1,$this->children,$this->level));
		    	$elements .= $this->objTpl->getResult(1);
		        $this->objTpl->unsetFile(1);
	        }
	        $this->setProperties($id);
	        $elements = $this->getParentTitle($id).$elements;
	       	return $elements;
	    }
    }
   	function drawThis() {
   		if($this->drawPageID) {
		    $this->setProperties($this->drawPageID);
		   	$elements = $this->drawSubpages($this->drawPageID);
		   	$this->setProperties($this->drawPageID);
		   	$tree = $this->drawTree($this->par_id);		    $this->setProperties($this->drawPageID);

		    //======================================================================
		       // SUB_PAGES
		    //======================================================================
		    $this->objTpl->setFile(1,'table-sub-pages.tpl');
		  	$this->objTpl->setVars(1,array('ELEMENTS'));
		    $this->objTpl->setValues(1,array($elements));
		    $sub_pages = $this->objTpl->getResult(1);
		    $this->objTpl->unsetFile(1);

		     if(empty($this->text)) {		       	$this->text = '��������, ������ �������� ��������� � ����������!';
		     }
		     $this->objTpl->setFile(1,'page.tpl');
		  	 $this->objTpl->setVars(1,array('ID','TITLE','TEXT','PATH','SUB_PAGES','TREE_PATH', 'HIERARCHY'));
		     $this->objTpl->setValues(1,array($this->id,$this->title,$this->text,$this->path, $sub_pages, 'http://'.$_SERVER['SERVER_NAME'].'/'.$this->getTree($this->par_id),$tree));
		     $content = $this->objTpl->getResult(1);
		     $this->objTpl->unsetFile(1);

		     $this->renderArray['title'] = $this->title;
		     $this->renderArray['content'] = $content;
        }
        else
        	$this->error = '����� �������� �� ����������';

    }
    function getParName($id) {    	  $sql = $this->objDb->query("select `TITLE` from `$this->table` where `ID`='$id'  and `LANG`='$this->language'");
    	 if($this->objDb->num_rows($sql)!=0)
    	 	return $this->objDb->result($sql,0);
    	 else
    	 	return '';
    }
    function definePage($path, $par_id) {
        $sql = $this->objDb->query("select `ID` from `$this->table` where `PATH`='".mysql_escape_string($path)."' and `PAR_ID` = '$par_id' and `LANG`='$this->language' LIMIT 1");
        if($this->objDb->num_rows($sql)) {
        	$this->drawPageID = $this->objDb->result($sql,0);
        }
    }
    function getPageId($array) {
  		foreach($array as $key=>$value) {
	   		if(!empty($value)) {
	     		$this->definePage($value, $this->drawPageID);
	     	}
	    }
	}
	function getPageById($id) {
    	$sql = $this->objDb->query("select * from `$this->table` where `ID`='$id' and `LANG`='$this->language'  limit 1");
        if($this->objDb->num_rows($sql)>0) {
	       	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
	       	return $row;
    	}
    	return 0;
    }
    function findRoot($id) {
       $sql = $this->objDb->query("select * from `$this->table` where `ID`='$id'   and `LANG`='$this->language' limit 1");
	   if($this->objDb->num_rows($sql)>0) {
		    $row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
		    $id =  $row["ID"];
		    if($row["PAR_ID"])
		     	$id =  $this->findRoot($row["PAR_ID"]);
		    return $id;
	   }
	  else return $id;
    }
    function setID($id) {
		$this->drawPageID = $id;
	}
	function getTree($id){
    	$sql = $this->objDb->query("select * from `$this->table` where `ID`='$id'  and `LANG`='$this->language' limit 1");
    	if($this->objDb->num_rows($sql)) {
	    	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
	    	$path=$row["PATH"];
	    	if($row["PAR_ID"]!=0)
	           $path =  $this->getTree($row["PAR_ID"]).'/'.$path;
	    	return $path;
    	}
    }
    function getParentTitle ($id)  {
	   $sql = $this->objDb->query("select * from `$this->table` where `ID`='$id'   and `LANG`='$this->language' limit 1");
	   if($this->objDb->num_rows($sql)) {
		    $row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
		    $path='<a style="text-transform: uppercase;font-weight: 900" href="{HTTP_ROOT}'.$this->module.'/'.$this->getTree($row["ID"]).'">'.$row["TITLE"].'</a> -> ';
		    if($row["PAR_ID"]!=0)
		     	$path =  $this->getParentTitle($row["PAR_ID"]).$path;
		    return $path;
	   }

	}
}
?>
