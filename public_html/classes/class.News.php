<?php
require_once $classes_dir.'class.Navigate.php';
require_once $classes_dir.'class.Date.php';

class News {
    var $page = 1;
	var $per_page = 10;
	var $from = 0;
	var $count = 0;
    function News($module, $moduleName){
        $this->objDb = new Database();
        $this->objTpl = new Templates();
        $this->objConf = new Config();
        $this->objData= new Data();
       	$this->module = ($module) ?  $module : 0;
	 	$this->moduleName = ($moduleName) ? $moduleName : 0;
	 	$this->table = $this->module;
	 	$this->language = $this->objConf->setLanguage();
        $this->page = (isset($this->objConf->arr['page'])) ?  $this->objConf->arr['page'] : 1;
        $this->newsID = (isset($this->objConf->arr['id'])) ? $this->objConf->arr['id'] : 0;
	}
    function setCount(){
     	$sql = $this->objDb->query("select count(*) from `$this->table` where `LANG`='$this->language'");
     	return $this->objDb->result($sql,0);
    }
	function setProperties($id){
    	$sql = $this->objDb->query("select * from `$this->table` where `ID`='$id' and `LANG`='$this->language'  limit 1");
    	if($this->objDb->num_rows($sql)>0) {
	       	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
	       	$this->id = $id;
	       	$this->title = $row["TITLE"];
	       	$this->stext = $row["STEXT"];
	       	$this->type = $row["TYPE"];
	       	$this->fpage = $row["FPAGE"];
	       	if(empty($this->stext)) {
	        	$this->stext = $this->objConf->lngPack[$this->language]["ent_empty"];
	        }
	        $this->ftext = $row["FTEXT"];
	        $this->date = $this->objDb->doutput($row["DATE"]);
	        return 1;
        }
        return 0;
    }
   	function drawThis() {
        if($this->newsID && !isset($this->num_printed)) {
         	if($this->setProperties($this->newsID)) {
		        $page_title = $this->title;
		        $this->objTpl->setFile(1,'news-full.tpl');
			  	$this->objTpl->setVars(1,array('ID','TITLE','STEXT','FTEXT','DATE'));
			    $this->objTpl->setValues(1,array($this->newsID,$this->title,$this->stext,$this->ftext, $this->objData->renderDate($this->date, '-')));
			    $this->renderArray['content'] = $this->objTpl->getResult(1);
			    $this->renderArray['title'] = $this->title;
	    	}
	    	else
	    		$this->error = '����� ������� �� ����������!';
        }
        else {
            $this->objNav = new Navigate($this->page,$this->per_page,$this->setCount(), $this->objConf->globals['http_root'].$this->module );
			$this->page = $this->objNav->page;
			$this->from = $this->objNav->from;

	        if(!isset($this->num_printed))
		  	$sql = $this->objDb->query("select `ID` from `$this->table` WHERE `LANG`='$this->language' ORDER BY `DATE` DESC limit $this->from,$this->per_page");
	    	else
	    		$sql = $this->objDb->query("select  `ID` from `$this->table` WHERE `LANG`='$this->language' ORDER BY `DATE` DESC limit $this->num_printed");

	    	for ($i=0;$i<$this->objDb->num_rows($sql);$i++)
	     		$this->renderArray['content'] .= $this->drawNewsElement($this->objDb->result($sql,$i));
		    if(empty($printArray['content']))
		    	$this->stext = $this->objConf->lngPack[$this->language]["ent_empty"];
		    $this->objTpl->setFile(1,'news.tpl');
		  	$this->objTpl->setVars(1,array('MODULE', 'NAVIGATE','ELEMENTS', 'COUNT'));
		    $this->objTpl->setValues(1,array($this->module, $this->objNav->drawThis() ,$this->renderArray['content'],$this->count));
		    $this->renderArray['content'] = $this->objTpl->getResult(1);
		    $this->renderArray['title'] = $this->moduleName;


        }
        $this->objTpl->unsetFile(1);
        $this->renderArray['module_name'] = $this->moduleName;
    }

    function drawLastNElemets ($num) {
    	$sql = $this->objDb->query("select * from `$this->table` WHERE `LANG`='$this->language' ORDER BY `DATE` DESC limit $num");
        for ($i=0;$i<$this->objDb->num_rows($sql);$i++)
	     	$return .= $this->drawNewsElement($this->objDb->result($sql,$i),$i+1);
	   	return $return;
    }
    function drawNewsElement($id) {
    	$this->setProperties($id);
    	if($this->type==1) {
    		$this->link = '<a href="{HTTP_ROOT}'.$this->module.'/id'.$id.'">���������</a>';
    	}
    	elseif($this->type==2) {
    		$this->link = '<a href="'.$this->fpage.'">���������</a>';
    	}
else
$this->link = '';
     	$this->objTpl->setFile(1,'news-elem.tpl');
	  	$this->objTpl->setVars(1,array('MODULE','ID','TITLE','STEXT','FTEXT','DATE','LINK'));
	   	$this->objTpl->setValues(1,array($this->module,$this->id,$this->title,$this->stext,$this->ftext,  $this->objData->renderDate($this->date, '-'),$this->link));
	   	$element = $this->objTpl->getResult(1);
	   	$this->objTpl->unsetFile(1);
	   	return $element;
    }
}

?>
