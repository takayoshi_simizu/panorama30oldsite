<?php
require_once $classes_dir.'class.Menu.php';
require_once $classes_dir.'class.StaticPage.php';
class Manager {
    function Manager(){
		$this->objDb = new Database();
        $this->objConf = new Config();
        $this->objTpl = new Templates();
        $this->objMenu = new Menu("menu");
        $this->objMenu2 = new Menu("menu2");
        $this->objAdd = new StaticPage("pages",'');
        $this->language = $this->objConf->setLanguage();
	}
 	function drawThis() {
 		ob_start();
  		session_start();
	    $this->objPrt = $this->objMenu->objPrt;
	   	$this->objPrt->drawThis();


	   	if(isset($this->objPrt->error) && !empty($this->objPrt->error)) {	   		$this->objPrt->renderArray['title'] = '��������!';
            $this->objPrt->renderArray['content'] = $this->objPrt->error;	   	}

        $this->objAdd->setID(6);
        $this->objAdd->drawThis();
        $left =  $this->objAdd->renderArray['content'];
        $this->objAdd->setID(7);
        $this->objAdd->drawThis();
        $right =  $this->objAdd->renderArray['content'];

   		$this->objMenu->drawThis();
   		$this->objMenu2->drawThis();
       	if(!isset($this->objPrt->renderedPage)) {
       		if($this->objMenu->main)
        		$this->objTpl->setFile(1,'index.tpl');
        	else
        		$this->objTpl->setFile(1,'index2.tpl');
			$this->objTpl->setVars(1,array('PAGE_TITLE','PAGE_CONTENT','MENU','MENU2','LEFT','RIGHT'));
			$this->objTpl->setValues(1,array($this->objPrt->renderArray['title'], $this->objPrt->renderArray['content'], $this->objMenu->renderedContent, $this->objMenu2->renderedContent,$left,$right));
	        $this->renderedPage = $this->objTpl->getResult(1);
		    $this->objTpl->unsetFile(1);
        }
        else {
        	$this->renderedPage = $this->objPrt->renderedPage;
        }
        return $this->objConf->replaceGlobals($this->renderedPage);
    }
    function setLanguage($lang) {
    	$_SESSION['language'] = $lang;
    }
    function renderVote() {    	require_once $classes_dir.'class.Vote.php';
    	$this->objVote = new Vote('vote','�����������');
    	$this->objVote->drawThis();    }
}
?>
