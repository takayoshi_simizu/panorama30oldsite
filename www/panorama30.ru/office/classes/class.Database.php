<?

////////////////////////////////////////////////////////////////////////////////
// ����� ��� ������ � ����� ������
////////////////////////////////////////////////////////////////////////////////

class Database {

	var $objConf;

	var $host;
	var $user;
	var $pass;
    var $name;

    function Database(){
		$this->host = 'localhost';
		$this->user = 'panorama_user';
		$this->pass = '123321';
		$this->name = 'panorama_cms';
		$this->connect();

		$this->objConf = new Config();
		$this->objTpl = new Templates();
    }

	function connect(){
		$link = mysql_connect($this->host, $this->user, $this->pass) or die ("Couldn`t connect to database");
		mysql_select_db($this->name, $link) or die ("Couldn`t connect to ".$this->name);
	}

    function query($query){
    	$sql = mysql_query($query) or die(mysql_error());
        return $sql;

    }

    function num_rows($result){
    	return mysql_num_rows($result);
    }

    function fetch_array($result){
		return mysql_fetch_array($result);
    }

    function result($result,$row,$field = ''){
		if (strlen(trim($field))>0)
			return mysql_result($result,$row,$field);
		else
			return mysql_result($result,$row);
    }

	function input($text){
    	$text = trim($text);
    	$text = strip_tags($text);
    	$text = addslashes($text);
    	return $text;
	}

	function ainput($arr){
		foreach ($arr as $key => $value){
			$text = $arr[$key];
   			$text = $this->input($text);
			$arr[$key] = $text;
		}
		return $arr;
	}

	function output($text){
		$text = stripslashes($text);
		return $text;
	}

	function aoutput($arr){
		foreach ($arr as $key => $value){
			$text = $arr[$key];
   			$text = $this->output($text);
			$arr[$key] = $text;
		}
		return $arr;
	}

	function doutput($date){
	    $mdate = explode("-", $date);
		return $mdate[2].'-'.$mdate[1].'-'.$mdate[0];
	}

	function dtoutput($datetime){
    	$mdate = explode(" ", $datetime);
    	$time = explode(":", $mdate[1]);
		return $time[0].':'.$time[1].' '.$this->doutput($mdate[0]);
	}

	function is_empty($text) {
		if(empty($text))
    		$text = $this->objConf->empty_feild;
  		return  $text;

	}
	function fillElem ($row, $tpl) {
		foreach($row as $key => $value) {
		 	if(is_integer(strpos($key, "DATE"))) {
		 		$row[$key] = $this->output($this->doutput($value));
		 	}

		}

       	$this->objTpl->setFile(1,$tpl);
		$this->objTpl->setVars(1,array_keys($row));
		$this->objTpl->setValues(1,array_values($row));
		$element = $this->objTpl->getResult(1);
		$this->objTpl->unsetFile(1);
		return $element;
	}

	function renderDate($date)       {
              $mdate = explode("-", $date);
	      return $mdate[0].' '.$this->renderMonth($mdate[1]).' '.$mdate[2];

	}
	function renderMonth($month) {
 switch($month) {
	case('01'):
		$month = '������';
		break;
	case('02'):
		$month = '�������';
		break;
	case('03'):
		$month = '�����';
		break;
	case('04'):
		$month = '������';
		break;
	case('05'):
		$month = '���';
		break;
	case('06'):
		$month = '����';
		break;
	case('07'):
		$month = '����';
		break;
	case('08'):
		$month = '�������';
		break;
	case('09'):
		$month = '��������';
		break;
	case('10'):
		$month = '�������';
		break;
	case('11'):
		$month = '������';
		break;
	case('12'):
		$month = '�������';
		break;
	}
	return $month;
}

function sqlMonth($month, $sql = false) {
	switch($month) {

	case('Jan'):
		$month = ($sql)? '01': '������';
		break;
	case('Feb'):
		$month = ($sql)? '02': '�������';
		break;
	case('Mar'):
		$month = ($sql)? '03': '�����';
		break;
	case('Apr'):
		$month = ($sql)? '04': '������';
		break;
	case('May'):
		$month = ($sql)? '05': '���';
		break;
	case('Jun'):
		$month = ($sql)? '06': '����';
		break;
	case('Jul'):
		$month = ($sql)? '07': '����';
		break;
	case('Aug'):
		$month = ($sql)? '08': '�������';
		break;
	case('Sep'):
		$month = ($sql)? '09': '��������';
		break;
	case('Oct'):
		$month = ($sql)? '10': '�������';
		break;
	case('Nov'):
		$month = ($sql)? '11': '������';
		break;
	case('Dec'):
		$month = ($sql)? '12': '�������';
		break;
	}
	return $month;
	}


    function getArr($name) {
    	$str= '';
		$arr = $this->objConf->unicodeArray(explode(',', $this->result($this->query("SELECT `VALUE` FROM params WHERE `NAME` = '$name'"),0)), 'windows-1251', 'utf-8');
		for ($i=0; $i<sizeof($arr); $i++) {
			   $str .=  '"'.trim($arr[$i]).'",';
		}
        return  'new Array('.substr($str,0,strlen($str)-1).')';
    }


    function getArrPHP($name) {
		$str = trim(iconv('utf-8','windows-1251',$this->getArr($name)));
		eval('$arr = '.substr($str,3,strlen($str)).';');
		$str = '<select name="'.strtolower($name).'">';
		for( $i=0; $i<sizeof($arr); $i++) {
			$str .= '<option value="'.$arr[$i].'">'.$arr[$i].'</option>';
		}
  		return $str.'</select>';
	}

    function getCalcElem ($sql) {
    	$content = '';

			$array = array();
			while($elem = $this->fetch_array($sql)) {
				$actions = '<img src="./design/edit.png" class="action" onclick="edit('.$elem['ID'].')"><img src="./design/delete.png"  class="action" onclick="remove('.$elem['ID'].')">';
				$elem['U_NAME'] = $this->getUserName($elem['U_ID']);
				$elem['ACTIONS'] = ((session_is_registered("user") and $elem['U_ID'] == $_SESSION['U_ID']) or session_is_registered("admin")) ? $actions : '';
				list($num,$num_) = explode('-',$elem['TIME']);
				$j=1;
				foreach($array as $key=>$value) {
					$arr_ = explode('7856',$key);
					if($arr_[0] == $num)
					$j++;
				}
				$array[$num.'7856'.$j] = $this->fillElem($elem,'calculate_elem.html');
			}
			ksort($array);
			foreach($array as $key=>$value) {
				$content .= $value;
			}
		return $content;
	}

	function getElem ($sql,$tpl,$del) {
    	$content = '';
		while($elem = $this->fetch_array($sql)) {
			$actions = ($del) ? '<img src="./design/edit.png" class="action" onclick="edit('.$elem['ID'].')"><img src="./design/delete.png"  class="action" onclick="remove('.$elem['ID'].')">' : '<img src="./design/edit.png" class="action" onclick="edit('.$elem['ID'].')">';
			$elem['U_NAME'] = $this->getUserName($elem['U_ID']);
	  		$elem['ACTIONS'] = ((session_is_registered("user") and $elem['U_ID'] == $_SESSION['U_ID']) or session_is_registered("admin")) ? $actions : '';
		    $content .= $this->fillElem($elem,$tpl);
		}
		return $content;

	}


    function getUserName($id) {
	   	$sql =  $this->query("SELECT `NAME` FROM `users` WHERE `ID` = '$id'");
		return ($this->num_rows($sql)>0) ? $this->result($sql,0) : '��������� ��������';
	}



}

?>