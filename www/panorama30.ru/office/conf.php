<?php
ob_start();
session_start();
error_reporting(0);
if(!session_is_registered("user") and !session_is_registered("admin")) {
    header("location: login.php");
}
$classes_dir = './classes/';
$d = opendir($classes_dir) or die("Couldn't open classes directory");
while(false !== ($f = readdir($d)))
	if(is_file("$classes_dir/$f")){
    	if (ereg("^class.[A-Za-z0-9.]*.php$",$f))
    		require_once "$classes_dir/$f";
 	}
$objConf = new Config();
$objDb = new Database();
if(!empty($_POST['search']) and $_POST['search']!= 'undefined')
	$search =  trim($_POST['search']);
else
	$search = false;
	if(isset($_POST['sdate']) and !isset($_POST['edate'])) {
	   $sdate =  $_POST['sdate'];
	   $edate = calcNextWeek($sdate);
	}
	elseif(!isset($_POST['sdate']) && isset($_POST['edate'])) {
	   $edate =  $_POST['edate'];
	   $sdate = calcPrevWeek($edate);
	}
	elseif(isset($_POST['sdate']) && isset($_POST['edate'])) {
		$sdate = $objDb->doutput($_POST['sdate']);
		$edate = $objDb->doutput($edate = $_POST['edate']);
	}
	else {
	   $sdate =  date("Y-m-d");
	   $edate = calcNextWeek($sdate);
	}


	if($sdate == $edate && $sdate == date("Y-m-d") )
		$title =  '�� ������� - '.$objDb->renderDate($objDb->doutput($sdate));
	else if($sdate == $edate )
		$title =  '�� '.$objDb->renderDate($objDb->doutput($sdate));
	else
		$title = 'C '.$objDb->renderDate($objDb->doutput($sdate)).' �� '. $objDb->renderDate($objDb->doutput($edate));



/*SPECILISTS--------------------------------------------------*/
$sql = $objDb->query("SELECT `VALUE` FROM `params` WHERE `NAME` = 'SPEC'");
$arrSpec = explode(',',$objDb->result($sql,0));
$num_calc = sizeof($arrSpec);
$specArr = array();
foreach($arrSpec as $key => $value) {
    // ������� ��� ��������� � ��������� ���������� ��� �����
    list($name, $hours_) = explode('(',$value);
    $name = trim($name);
    // ������ ����������
    $hours__ = explode('|',substr($hours_,0,strlen($hours_)-1));
    // ��������� ������
    // $specArr [ ��� ���������] = [8,9,10,11 ...]
    $specArr[$name] = array();
    foreach($hours__ as $_key => $_value) {       list($start, $end) = explode("-", $_value);
       for($start; $start<$end; $start++)
      	 array_push($specArr[$name],$start);
    }}
$str= '';
$specArr_ = array_keys($specArr);
for ($i=0; $i<sizeof($specArr_); $i++) {
	$str .=  '"'.trim($specArr_[$i]).'",';
	$str_ .= '<option value="'.$specArr_[$i].'">'.$specArr_[$i].'</option>';

}
$js_spec = iconv('windows-1251', 'utf-8','new Array('.substr($str,0,strlen($str)-1).')');
$spec = '<select id="spec" onChange="getTime()" name="spec">'.substr($str_,0,strlen($str_)-1).'</select>';



$offers_table = 'offers';
if(isset($_GET['page']))
	$page = (int) $_GET['page'];
else
	$page = 1;
$from = 0;
$per_page = 25;

// CALENDAR FUNCTIONS
function isLeapYear($y) {
    return ($y%4==0 && $y%100!=0) || $y%400==0;
}
function getDayInMonth($month,$year) {
	if(strlen($month) == 1)
		$month = '0'.$month;	$arr = Array("01"=>31,"02"=>(isLeapYear($year) ? 29:28),"03"=>31,"04"=>30,"05"=>31,"06"=>30,"07"=>31,"08"=>31,"09"=>30,"10"=>31,"11"=>30,"12"=>31);
	return $arr[$month];
}
function calcNextWeek ($date) {	 list($year,$month,$day) = explode('-',$date);     $days =  getDayInMonth($month, $year);
     if(($day+6)>$days) {
   		$day = ($day+6)-$days;
   		if($month<12) {
   			$month++;
   		}
   		else {
   			$month = 01;
   			$year++;
   		}

	 }
	 else {	       $day += 6;
	 }
	 return   $year.'-'.((strlen($month)==1) ?  '0'.$month :  $month).'-'.((strlen($day)==1) ?  '0'.$day :  $day);

}
function calcPrevWeek ($date) {
	 list($year,$month,$day) = explode('-',$date);
     if(($day-6)<1) {
   		if($month>1) {
   			$month--;
   		}
   		else {
   			$month = 12;
   			$year--;
   		}
        $days =  getDayInMonth($month, $year);
        $day = $day-6+$days;
	 }
	 else {
	       $day -= 6;
	 }
	 return   $year.'-'.((strlen($month)==1) ?  '0'.$month :  $month).'-'.((strlen($day)==1) ?  '0'.$day :  $day);
}
function calcPrevDay ($date) {
	 list($year,$month,$day) = explode('-',$date);
     if(($day-1)<1) {
   		if($month>1) {
   			$month--;
   		}
   		else {
   			$month = 12;
   			$year--;
   		}
        $days =  getDayInMonth($month, $year);
        $day = $day-1+$days;
	 }
	 else {
	       $day -= 1;
	 }
	 return  $year.'-'.((strlen($month)==1) ?  '0'.$month :  $month).'-'.((strlen($day)==1) ?  '0'.$day :  $day);
}
?>
