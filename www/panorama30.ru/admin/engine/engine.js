//��������� ������--------------------------------------------------------------
function stopError() {
  return true;
}
//==============================================================================
// ERROR
//==============================================================================
function changeTreePath(objFrom,  idTo, preTxt) {
	document.getElementById('doc_tree_'+idTo).innerText = preTxt+'/'+objFrom.value;
	slideDown( document.getElementById('doc_tree_'+idTo) );
}
//==============================================================================
// FORM
//==============================================================================
function check(obj) {
 error_full = 0;
 arrInput = obj.getElementsByTagName('input')
 var arrChkInp = [];
 for(i=0; i<arrInput.length;i++) {
 	if(arrInput[i].getAttribute('check') == 'true') {
 		 if(arrInput[i].value.length == 0) {
 		 	error_full = 1;
 		 }

 	}
 	if(arrInput[i].getAttribute('type') == 'submit')
 		 	submit = arrInput[i]
 }

 if(error_full) {
 	document.getElementById('error_txt').innerHTML = '�� ��� ���� ����������!';
 	submit.className =  'dis_submit';
    submit.setAttribute("disabled","disabled")
 }
 else  {
 	document.getElementById('error_txt').innerHTML = '';
 	submit.className =  'submit';
 	submit.removeAttribute("disabled")
 }

}
//==============================================================================
// MOUSE POSITION (CROSSED)
//==============================================================================
function getMouseX(e) {
	e = e || window.event;
	return e.pageX || e.clientX + document.body.scrollLeft;
}

function getMouseY(e) {
	e = e || window.event;
	return e.pageY || e.clientY + document.body.scrollTop;
}
function getElementX( e ) {
	return ( e && e.layerX ) || window.event.offsetX;
}
function getElementY( e ) {
	return ( e && e.layerY ) || window.event.offsetY;
}
//==============================================================================
// HELP
//==============================================================================
function showHelp(text) {
	helpObj = document.getElementById("help");
	show(helpObj);
	helpObj.innerHTML = text;
	helpObj.style.left = getMouseX() - 8;
	helpObj.style.top = getMouseY() + 18;
	window.event.srcElement.onmouseout = function () {		hide(helpObj)
		// IF WYSIWYG TOOL =================================
		if(this.className=='tool_rollover')
			rollOutWisTool(this)
	}
}
//==============================================================================
// SLIDE,FADE,ETC ELEMENT
//==============================================================================
function slideDown( elem ) {	elem.style.height = '0px';
	show( elem );
	var h = fullHeight( elem );
	for ( var i = 0; i <= 100; i += 5 ) {		function() {
			var pos = i;
			setTimeout( function(){				elem.style.height = ((pos/100)*h) + "px";
			}, (pos+1)*100  );
		}
    }
}
//==============================================================================
// SHOW / HIDE ELEMENT
//==============================================================================
function show( elem ) {
	elem.style.display = elem.$oldDisplay || '';
	elem.style.display = '';
}
function hide( elem ) {
	var curDisplay = getStyle( elem, 'display' );
	if ( curDisplay != 'none' )
		elem.$oldDisplay = curDisplay;
	elem.style.display = 'none';
}
//==============================================================================
// HEIGHT / WIDTH ELEMENT (CROSSED)
//==============================================================================
function fullHeight( elem ) {
	// If displayed
	if ( getStyle( elem, 'display' ) != 'none' )
		return elem.offsetHeight || getHeight( elem );
	// If non displayed
	var old = resetCSS( elem, {
		display: '',
		visibility: 'hidden',
		position: 'absolute'
	});
	var h = elem.clientHeight || getHeight( elem );
	restoreCSS( elem, old );
	return h;
}

function fullWidth( elem ) {
	if ( getStyle( elem, 'display' ) != 'none' )
		return elem.offsetWidth || getWidth( elem );
	var old = resetCSS( elem, {
		display: '',
		visibility: 'hidden',
		position: 'absolute'
	});
	var w = elem.clientWidth || getWidth( elem );
	restoreCSS( elem, old );
	return w;
}

function getHeight( elem ) {
	return parseInt( getStyle( elem, 'height' ) );
}

function getWidth( elem ) {
	return parseInt( getStyle( elem, 'width' ) );
}
//==============================================================================
// CSS (CROSSED)
//==============================================================================
function resetCSS( elem, prop ) {
	var old = {};
	for ( var i in prop ) {
		old[ i ] = elem.style[ i ];
		elem.style[ i ] = prop[i];
	}
	return old;
}
function restoreCSS( elem, prop ) {
	for ( var i in prop )
	elem.style[ i ] = prop[ i ];
}
function changeClass(obj,newClass) {	oldClass = obj.className;
    obj.className = newClass;
	return oldClass;
}
function resetClass(obj, oldClass) {
	obj.className = oldClass

}
function getStyle( elem, name ) {
	// If style[]
	if (elem.style[name])
		return elem.style[name];
	// IE
	else if (elem.currentStyle)
		return elem.currentStyle[name];
	// W3C
	else if (document.defaultView && document.defaultView.getComputedStyle) {
		// It uses the traditional 'text-align' style of rule writing,
		// instead of textAlign
		name = name.replace(/([A-Z])/g,"-$1");
		name = name.toLowerCase();
		// Get the style object and get the value of the property (if it exists)
		var s = document.defaultView.getComputedStyle(elem,"");
		return s && s.getPropertyValue(name);
		// Otherwise, we're using some other browser
	}
	else {		return null
	}
}
//==============================================================================
// HIGHLIGHT
//==============================================================================
function  highlight( obj, newClass ) {
	if(pressed_el!=obj) {	    oldClass = changeClass(obj, newClass);
	    if(obj.tagName.toLowerCase()=='input') {
	    	obj.onblur = function () {
	         	resetClass(this, oldClass)
			}
		}
		else {			obj.onmouseout = function () {
	         	resetClass(this, oldClass)
			}
		}
	}
}
//==============================================================================
// OPACITY (CROSSED)
//==============================================================================
function setOpacity( elem, level ) {
    // IE
	if ( !elem.style.opacity )
		elem.style.filters = 'alpha(opacity=' + level + ')';
	// W3C
	else
		elem.style.opacity = level / 100;
}
//==============================================================================
// SWITCH CONTENT (CROSSED)
//==============================================================================
function switchContent( elem ) {	arr = document.getElementById("contTable").getElementsByTagName("div")
   	for(var i in arr) {
   		if(arr[i].className == 'contMenuActive')
   			break;
   	}
   	arr[i].className ='contMenu';
   	arr[i].onmouseover = function(){highlight(this, 'contMenuHover')}
   	arr[i].onclick = function(){ switchContent( this )}
   	hide(document.getElementById(arr[i].id+'Cont'))
   	elem.onclick = elem.onmouseout = elem.onmouseover = null;
	elem.className = 'contMenuActive';
   	show(document.getElementById(elem.id+'Cont'))
}
function pressElement(id) {	if(pressed_el != document.getElementById(id)) {		if(pressed_el != null)			changeClass(pressed_el, '');		pressed_el = document.getElementById(id)
		pressed_el.onmouseout = null;		changeClass(pressed_el, 'pressed');
		if(exPr = document.getElementById('ifExPr')) {			setOpacity( exPr, 100 )
			a = exPr.getElementsByTagName("a");
			for (var i = 0; i < a.length; i++ ) {
				/*a[i].onclick= function(e) {					return false;
				}*/
				a[i].style.cursor = "hand"
			}

		}
	}
	else {
		changeClass(document.getElementById(id), '');		pressed_el = 0;
		if(exPr = document.getElementById('ifExPr')) {
			setOpacity( exPr, 20 )
			for (var i = 0; i < a.length; i++ ) {
				/*a[i].onclick= function(e) {
					return false;
				} */
				a[i].style.cursor = "text"
			}

		}
	}




}
/*
REDIRECT
*/
pressed_el = 0;
function go(url) {
	window.location.href = url;
	location.replace(url)
	location = url;
}
function deleteItem(url) {
	if(pressed_el.id!=null)
		if(confirm("�������?"))
			go(url + pressed_el.id)
		else
			return null
	else
		alert("������ �� �������!")
}
function deleteItema(url) {
		if(confirm("�������?"))
			go(url)
		else
			return null
}
function editItem(url) {
	if(pressed_el.id!=null)
		go(url+pressed_el.id)
	else
		alert("������ �� �������!")
}

/*
DOM READY
*/

function domReady( f ) {
	if ( domReady.done ) return f();
		if ( domReady.timer ) {
			domReady.ready.push( f );
		} else {
		window.load = isDOMReady || addEvent(window, "load", isDOMReady)
		domReady.ready = [ f ];
		domReady.timer = setInterval( isDOMReady, 13 );
	}
}
function isDOMReady() {
	if ( domReady.done ) return false;
		if (document && document.getElementsByTagName && document.getElementById && document.body ) {
			clearInterval( domReady.timer );
			domReady.timer = null;
			for ( var i = 0; i < domReady.ready.length; i++ )
			domReady.ready[i]();
			domReady.ready = null;
			domReady.done = true;
	}
}


domReady(function() {
	div = document.getElementsByTagName("div");
	for(i in div) {
	    if(div[i].rel!=null) {
	    	div[i].ondblclick = function() {	    		//document.selection.empty()
	    		go(this.rel)
	    		highlight(this, 'pressed')


	    	}

	    	//div[i].attachEvent("onmouseover", function {document.onselectionchange = document.selection.empty});
	    	//goChildren (div[i]);

	    }
	}
	hide(document.getElementById("help"));
	//hide(document.getElementById("preload"));
	//setOpacity(document.getElementById("content"), 100)
});




function goChildren (e) {	e = e.firstChild;	while(e!=null) {		if(e.onselect )	    e.onselect = function() {			alert("!")
			//document.selection.empty()
	    }
		if(e.childNodes)
			goChildren(e);
		e = e.nextSibling
	}

}




//timerID = setTimeout("preLoadComplete()",2000)
//window.onerror = stopError;
//window.onload = preLoadComplete;




