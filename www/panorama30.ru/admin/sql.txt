
CREATE TABLE `pages` (
  `ID` int(11) NOT NULL auto_increment,
  `TITLE` varchar(100) default '��� ��������',
  `TEXT` text NOT NULL,
  `PATH` varchar(30) NOT NULL,
  `PAR_ID` smallint(6) NOT NULL,
  `LANG` varchar(10) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;
        

CREATE TABLE `news` (
  `ID` smallint(6) NOT NULL auto_increment,
  `TITLE` varchar(200) NOT NULL,
  `STEXT` text NOT NULL,
  `TYPE` varchar(1) NOT NULL default '0',
  `FTEXT` text NOT NULL,
  `DATE` date NOT NULL,
  `FPAGE` varchar(200) default NULL,
  `LANG` varchar(10) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;
        


CREATE TABLE `gallery_albums` (
  `ID` int(5) NOT NULL auto_increment,
  `TITLE` varchar(200) default NULL,
  `TEXT` text NOT NULL,
  `DATE` date default NULL,
  `PAR_ID` smallint(6) NOT NULL default '0',
  `LANG` varchar(10) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;
        

CREATE TABLE `catalog_categoris` (
  `ID` int(5) NOT NULL auto_increment,
  `TITLE` varchar(200) default NULL,
  `PAR_ID` int(5) default '0',
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;


CREATE TABLE `catalog_products` (
  `ID` smallint(6) NOT NULL auto_increment,
  `TITLE` varchar(200) NOT NULL,
  `TEXT` text NOT NULL,
  `PRICE` smallint(200) default '0',
  `A_ID` smallint(6) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

CREATE TABLE `menu` (
  `ID` smallint(6) NOT NULL auto_increment,
  `M_ID` varchar(100) NOT NULL,
  `POS` smallint(6) NOT NULL,
  `LANG` varchar(10) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=cp1251 AUTO_INCREMENT=64 ;
        

      