<?php

class StaticPageAdmin extends StaticPage {    function StaticPageAdmin($module, $moduleName){        $this->StaticPage($module, $moduleName);
        $this->objDb = new DatabaseAdmin();
        $this->objTpl = new TemplatesAdmin();
        $this->objConf = new ConfigAdmin();
        $this->objURL = new URL();


        $this->drawPageID = (isset($this->objConf->arr['id']) ) ?  $this->objConf->arr['id'] : 0;
	}
	function getParentTitle ($id)  {
	   $sql = $this->objDb->query("select * from `$this->table` where `ID`='$id'   and `LANG`='$this->language' limit 1");
	   if($this->objDb->num_rows($sql)) {
		    $row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
		    $path='<a style="text-transform: uppercase" href="{HTTP_ROOT}'.$this->module.'/'.$this->getTree($row["ID"]).'">'.$row["TITLE"].'</a> -> ';
		    if($row["PAR_ID"]!=0)
		     	$path =  $this->getParentTitle($row["PAR_ID"]).$path;
		    return $path;
	   }
	}
    function drawSubpages($id) {    	$elements='';
   		$sql = $this->objDb->query("select `ID` from `$this->table` where `PAR_ID`='$id' and `LANG`='$this->language'");
   		if($this->objDb->num_rows($sql)!=0) {
	        for ($i=0;$i<$this->objDb->num_rows($sql);$i++)  {
		        $this->setProperties($this->objDb->result($sql,$i));
		 	    $this->objTpl->setFile(2,'sub-pages-elem.tpl');
		  		$this->objTpl->setVars(2,array('MODULE','ID','TITLE','TREE_PATH','PATH','NUM', 'TYPE'));
		    	$this->objTpl->setValues(2,array($this->module,$this->subpage_id,$this->title,$this->tree_path,$this->path, $i+1,$this->children));
		    	$elements .= $this->objTpl->getResult(2);
		    	$this->objTpl->unsetFile(2);
	        }
        }
       	else
        	$elements = '<div class="sectionEpmty">���������� ���!</div>';
        return $elements;
    }

   	function drawThis() {
		if($this->objConf->query[1]=='add' && isset($this->drawPageID)) {			$par_id = (int)$this->drawPageID;
           	$sql = $this->objDb->query("SELECT `ID` FROM `$this->table` ORDER BY `ID` DESC LIMIT 1");
			$id = $this->objDb->result($sql,0)+1;
			$this->objDb->query("INSERT `$this->table` VALUES('$id','-- ��� ��������$id','','page$id','$par_id','$this->language')");
			header("location: ".$this->objConf->globals['http_root'].$this->module.'/'.$this->getTree($par_id));
		}
		elseif(isset($this->objConf->arr['delete']) && isset($this->drawPageID)) {
		  	$id = $this->objDb->result($this->objDb->query("SELECT `PAR_ID` FROM `$this->table` WHERE `ID` = '$this->drawPageID' LIMIT 1"),0);
		  	$id = ($id!=0) ?  '/id'.$id : '';
		  	$this->delete($this->drawPageID);
			header("location: ".$this->objConf->globals['http_root'].$this->module.$id);
		}
		elseif (isset($this->objConf->arr['edit']) && isset($this->drawPageID) && isset($_POST['title']) && isset($_POST['text']) && isset($_POST['path'])) {
		    $title = $_POST['title'];
		    $text =  $_POST['text'];
		    $path = $_POST['path'];
			$this->objDb->query("UPDATE `$this->table` SET `TITLE`='$title', `TEXT`='$text', `PATH`='$path' WHERE `ID` = '$this->drawPageID'");
		    header("location: ".$this->objConf->globals['http_root'].$this->module.'/'.$this->getTree($this->drawPageID));
		}
		else {	        if($this->drawPageID) {		        $this->sub_pages = $this->drawSubpages($this->drawPageID);
		        $this->setProperties($this->drawPageID);
		        //if($this->par_id ==0)
			 	// 	$disabled='disabled=disabled';
			 	//else
			 		$disabled='';
		       	$this->objTpl->setFile(1,'page.tpl');
		  		$this->objTpl->setVars(1,array('MODULE', 'ID','PAGE_TITLE','TEXT','PATH','SUB_PAGES','TREE_PATH','DISABLED'));
		    	$this->objTpl->setValues(1,array($this->module, $this->drawPageID,$this->objConf->wys_ready($this->title),$this->objConf->wys_ready($this->text),$this->path, $this->sub_pages, 'http://'.$_SERVER['SERVER_NAME'].'/'.$this->getTree($this->par_id),$disabled));
		    	$this->renderArray['content'] = $this->objTpl->getResult(1);
		    	$this->renderArray['title'] = '<a href="{HTTP_ROOT}'.$this->module.'/">�������</a>->'.$this->getParentTitle($this->par_id).$this->title;
		    	$this->renderArray['page_error'] = $this->error;
	        }
	        else {
		       	$this->objTpl->setFile(1,'pageall.tpl');
		  		$this->objTpl->setVars(1,array('MODULE','PAGE_TITLE','SUB_PAGES'));
		    	$this->objTpl->setValues(1,array($this->module,'�������', $this->drawSubpages(0)));
		    	$this->renderArray['content'] = $this->objTpl->getResult(1);
		    	$this->renderArray['title'] = '�������';
		    	$this->renderArray['page_error'] = $this->error;
	        }
	        $this->objTpl->unsetFile(1);
            $this->renderArray['module_name'] = $this->moduleName;
        }
    }
    function delete($id) {
		$this->objDb->query("DELETE FROM `$this->table` WHERE `ID` = '$id' LIMIT 1");
		$sql = $this->objDb->query("SELECT `ID` FROM `$this->table` WHERE `PAR_ID` = '$id'  and `LANG`='$this->language'");
		for ($i=0;$i<$this->objDb->num_rows($sql);$i++)
			$this->delete($this->objDb->result($sql, 0));
    }
    function drawPages($id) {
   		$sql = $this->objDb->query("select * from `$this->table` where `PAR_ID`='$id' and `LANG`='$this->language'");
	    for ($i=0;$i<$this->objDb->num_rows($sql);$i++)
		   $elements[] = $this->objDb->aoutput($this->objDb->fetch_array($sql));
        return $elements;
    }


}

?>
