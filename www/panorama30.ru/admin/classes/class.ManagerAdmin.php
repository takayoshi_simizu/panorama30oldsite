<?php
require_once $classes_dir.'class.MenuAdmin.php';
class ManagerAdmin extends Manager {
     function ManagerAdmin(){
        $this->objDb = new DatabaseAdmin();
        $this->objTpl = new TemplatesAdmin();
        $this->objConf = new ConfigAdmin();
        $this->objMenu = new MenuAdmin("menu");

        $this->login = 'admin_pan';
        $this->pass = 'napnap2011';

        $this->table = 'mcms_modules';
        $this->language = $this->objConf->setLanguage();
	}
    function getTree($id){
    	$sql = $this->objDb->query("select * from `$this->table` where `ID`='$id'  limit 1");
    	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
    	$path=$row["PATH"];
    	if($row["PAR_ID"]!=0)
           $path =  $this->getTree($row["PAR_ID"]).'/'.$path;
    	return $path;
    }
    function drawMenu($par_id, $level) {
    	$elements='';
 		$sql = $this->objDb->query("select * from `$this->table` where `LANG`='$this->language'  AND `PRINTED`=1 order by `ID`");
        for($i=1; $i<=$this->objDb->num_rows($sql);$i++) {
        	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
	       	$tree_path = $this->getTree($row["ID"]);
        	//======================================================================
	        // LEVEL
	        //======================================================================
        	$tr_level='' ;
        	for($j=1; $j <= $level;$j++)
        		$tr_level.='<td width=10></td>';
            //======================================================================
	        // IF ELEMENT IS LAST
	        //======================================================================
	        if($i==$this->objDb->num_rows($sql))
	        	$pos='last';
	        else
	        	$pos='';
	        //======================================================================
	        // FORMING ELEMENT MENU
	        //======================================================================
            if($row["ID"]==$this->page_id)
            	$this->objTpl->setFile(2,'mod-elem-a.tpl');
            else
	 	    	$this->objTpl->setFile(2,'mod-elem.tpl');
	  		$this->objTpl->setVars(2,array('ID','TITLE','TREE_PATH', 'POS', 'LEVEL'));
	    	$this->objTpl->setValues(2,array($row["ID"],$row["TITLE"],$tree_path, $pos, $tr_level));

	    	$elements .= $this->objTpl->getResult(2);
	        $this->objTpl->unsetFile(2);
	        //======================================================================
	        // CHECK CHILDREN - RECURSION
	        //======================================================================
	        //$sql_children = $this->objDb->query("select * from `$this->table` where  `LANG`='$this->language' order by `ID`");
	        //if($this->objDb->num_rows($sql_children)!=0)
	        //	$elements .= $this->drawMenu($row["ID"], $level+1);
        }
        return $elements;

    }
   	function drawThis() {
   		ob_start();
   		session_start();
   		
   		if(session_is_registered("admin")) {
	   		if(isset($this->objConf->query[0])) {
	   			if($this->arrQuery[0]=='mcms_modules')
	   				$this->printArray = $this->objMod->drawThis();
	   			elseif($this->objConf->query[0]=='changelang') {
	   				$this->setLanguage($this->arrQuery[1]);
	   			}
	   			elseif($this->objConf->query[0]=='logout') {
	   				session_destroy();
	   				header("location: ./");
	   			}
	   			else {
	   				$this->objPrt = $this->objMenu->objPrt;
	   				$this->objPrt->drawThis();
	   			}
	   		}
	   		//======================================================================
	        // IF PAGE NOT EXIST
	        //======================================================================
	   		if (!empty($this->printArray['page_error'])) {
	   			$this->objTpl->setFile(1,'error.tpl');
		  		$this->objTpl->setVars(1,array('PAGE_ERROR'));
		    	$this->objTpl->setValues(1,array($this->printArray['page_error']));
		    	$this->printArray['page_error'] = $this->objTpl->getResult(1);
	        	$this->objTpl->unsetFile(1);
	        	$this->printArray['page_content'] = '';
	        	$this->printArray['page_title'] = '������';
		    }






		    //======================================================================
	        // FORMING PAGE
	        //======================================================================
	   		$this->objTpl->setFile(1,'main.tpl');
	  		$this->objTpl->setVars(1,array('MENU','MODULE_NAME','PAGE_TITLE','PAGE_ERROR', 'PAGE_CONTENT'));
	    	$this->objTpl->setValues(1,array($this->drawMenu(0,0), $this->objPrt->renderArray['module_name'], $this->objPrt->renderArray['title'],$this->objPrt->renderArray['page_error'],$this->objPrt->renderArray['content']) );
	    	$content = $this->objTpl->getResult(1);
	        $this->objTpl->unsetFile(1);

	        $this->objTpl->setContent(1,$content);
	  		$this->objTpl->setVars(1,array('MENU','MODULE_NAME','PAGE_TITLE','PAGE_ERROR', 'PAGE_CONTENT'));
	    	$this->objTpl->setValues(1,array($this->drawMenu(0,0), $this->objPrt->renderArray['module_name'], $this->objPrt->renderArray['title'],$this->objPrt->renderArray['page_error'],$this->objPrt->renderArray['content']) );
	    	$content = $this->objTpl->getResult(1);
	        $this->objTpl->unsetFile(1);
        }
        else {
        	if(isset($_POST['login']) &&  isset($_POST['pass'])) {
        		if($_POST['login']==$this->login && $_POST['pass']==$this->pass) {
        			 session_register("admin");
        			 header("location: ./");

        		}
        	}
        	$this->objTpl->setFile(1,'login.tpl');
	  		$this->objTpl->setVars(1,array());
	    	$this->objTpl->setValues(1,array());
	    	$content = $this->objTpl->getResult(1);
	        $this->objTpl->unsetFile(1);
        }
		return $this->objConf->replaceGlobals($content);
        ob_end_flush();

    }
    function setLanguage($lang) {
    	$_SESSION['language'] = $lang;
    	header("location: ".$this->objConf->globals['http_root']);
    }
}
?>
