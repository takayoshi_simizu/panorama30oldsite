<?php

class FieldAdmin extends Field {

     function FieldAdmin(){        $this->Field();

        $this->objDb = new DatabaseAdmin();
        $this->objTpl = new TemplatesAdmin();
        $this->objConf = new ConfigAdmin();

	}

   	 function drawThis() {   		$printArray = array();        $this->setProperties('5');

       	$this->objTpl->setFile(1,'field.tpl');
  		$this->objTpl->setVars(1,array('MODULE', 'ID','PAGE_TITLE','TEXT'));
    	$this->objTpl->setValues(1,array($this->module, $this->id,$this->title,$this->objConf->wys_ready($this->text)));


    	$printArray['module_name'] = $this->moduleName;
    	$printArray['page_content'] = $this->objTpl->getResult(1);
    	$printArray['page_title'] = $this->title;
    	$printArray['page_error'] = $this->error;
        $this->objTpl->unsetFile(1);


        return $printArray;
    }
}

?>
