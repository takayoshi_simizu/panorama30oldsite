<?php
class GalleryAdmin extends Gallery {
     function GalleryAdmin($module, $moduleName){
    	$this->Gallery($module, $moduleName);
        $this->objDb = new DatabaseAdmin();
        $this->objTpl = new TemplatesAdmin();
        $this->objConf = new ConfigAdmin();
        $this->objIMG = new IMG();
        $this->objFileSystem = new FileSystem();
        $this->path = $this->objConf->globals['root'].'/content/'.$this->module.'/'.$this->albumID.'/';
	}
     function drawThis(){
        if(isset($this->objConf->arr['addalbum']) && isset($this->albumID)) {
 			$sql = $this->objDb->query("SELECT `ID` FROM `$this->album_table` ORDER BY `ID` LIMIT 1");
			if($this->objDb->num_rows($sql)>0)
				{$id=$this->objDb->result($sql,0)+1;}
			else
				{$id =1;}
			$this->objDb->query("INSERT `$this->album_table` VALUES(null,'��� ��������', '','".date('Y-m-d')."','$this->albumID' ,'$this->language')");
			header("location: ".$this->objConf->globals['http_root'].$this->module.'/album'.$this->albumID);
        }
        elseif(isset($this->objConf->arr['deletealbum']) && $this->albumID) {
			$this->delete($this->albumID);
			header("location: ". $this->objConf->globals['http_root'].$this->module);
        }
        elseif(isset($this->objConf->arr['editalbum']) && $this->albumID) {
        	if(isset($_POST['title']) and isset($_POST['date'])) {
	            if(isset($_FILES['userfile'])) {

	            	$path = $this->path.'thumbs/';

$this->objFileSystem->makedir($this->path);
$this->objFileSystem->makedir($path);
	            	$new_name =  $this->objIMG->translit($_FILES['userfile']['name']);
	                $this->objIMG->upload('userfile',$new_name, $this->path);
					$this->objFileSystem->copyfile($this->path.$new_name,$path.$new_name);
					$this->objIMG->resizeImg($path.$new_name);
				}
			}
			// EDIT INFO ***************************************************************
		    $d = explode('.',$_POST['date']);
		    $date = $d[2].'-'.$d[1].'-'.$d[0];
		    $text = mysql_escape_string($_POST['text']);
		    $title = mysql_escape_string($_POST['title']);

			$this->objDb->query("UPDATE `$this->album_table` SET `TITLE`='$title', `DATE`='$date', `TEXT` = '$text' WHERE `ID` = '$this->albumID' ");
		    // REDIRECT ****************************************************************
		    header("location: ".$this->objConf->globals['http_root'].$this->module.'/album'.$this->albumID);

        }
        elseif(isset($this->objConf->arr['deletephoto'])) {
        	$id = $this->objConf->arr['id'];
        	$file = str_replace("'",'',stripslashes($_GET['photo']));
			$img_path = $this->objConf->globals['root'].'/content/'.$this->module.'/'.$id.'/'.$file;
			$img_thump_path = $this->objConf->globals['root'].'/content/'.$this->module.'/'.$id.'/thumbs/'.$file;
			$this->objFileSystem->removefile($img_path);
			$this->objFileSystem->removefile($img_thump_path);
			header("location: ".$this->objConf->globals['http_root'].$this->module.'/album'.$id);
        }
        elseif(isset($this->objConf->arr['place']) && isset($this->objConf->arr['id']) && isset($this->albumID)){
        	$id = $this->objConf->arr['id'];
        	$this->objDb->query("UPDATE `$this->album_table` SET `PAR_ID`='$id' WHERE `ID` = '$this->albumID'");
        	header("location: ".$this->objConf->globals['http_root'].$this->module.'/album'.$id);
        }
 		elseif($this->albumID) {
            $this->printedAlbumId = $this->albumID;
 			$photos = $this->drawPhotos();
            $albums = $this->drawAlbums($this->printedAlbumId);
            $this->setAlbumProperties($this->printedAlbumId);

			$this->objTpl->setFile(1,'album.tpl');
			$this->objTpl->setVars(1,array('MODULE', 'MODULE_NAME','ID','TITLE','DATE', 'TEXT','PAGE_TITLE', 'PHOTOS', 'ALBUMS'));
			$this->objTpl->setValues(1,array($this->module, $this->moduleName, $this->albumID,$this->title,str_replace('-','.',$this->objDb->doutput($this->date)),$this->objConf->wys_ready($this->text), $this->title, $photos, $albums));
		 	$this->renderArray['content'] = $this->objTpl->getResult(1);
			$this->renderArray['title'] = $this->getParentTitle($this->par_id).$this->title;
			$this->objTpl->unsetFile(1);
        }
        else {
	        $albums = $this->drawAlbums(0);

	 		$this->objTpl->setFile(1,'gallery.tpl');
	  		$this->objTpl->setVars(1,array('MODULE','ALBUMS', 'PAGE_TITLE'));
	        $this->objTpl->setValues(1,array($this->module,$albums, $page_title));
	        $this->renderArray['content'] = $this->objTpl->getResult(1);
		    $this->renderArray['title'] = '�������';
	        $this->objTpl->unsetFile(1);
        }
        $this->renderArray['module_name'] = $this->moduleName;
    }

	  function drawAlbums($par_id) {
 		$albums = '';
   		$sql = $this->objDb->query("select `ID` from `$this->album_table` WHERE `PAR_ID`='$par_id' AND `LANG` = '$this->language' order by `DATE`");
	   	for ($i=0;$i<$this->objDb->num_rows($sql);$i++)
	    	$albums .= $this->drawAlbumElement($this->objDb->result($sql,$i),$i+1);
	    if(empty($albums))
	    	$albums = '<div class="sectionEpmty">�������� ���</div>';
	    return $albums;
	}
	  function drawAlbumElement($id,$i){
        $this->options = $this->drawMap(0);
 		$sql = $this->objDb->query("select * from `$this->album_table` where `ID`='$id'  limit 1");
       	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
   		$this->objTpl->setFile(1,'gallery-elem.tpl');
  		$this->objTpl->setVars(1,array('MODULE','ID','IMG','TITLE','DATE', 'NUM', 'OPTIONS'));
    	$this->objTpl->setValues(1,array($this->module,$id,'',$row["TITLE"],$this->objDb->doutput($row["DATE"]), $i, $this->options));
    	$content = $this->objTpl->getResult(1);
        $this->objTpl->unsetFile(1);
        return $content;
	}
	function drawPhotos() {
	 	if(!is_dir($this->path))
			mkdir($this->path, 0777);
		$photos='';
	 	$d = opendir($this->path) or die("Couldn't open images directory");
		$i=1;
		while(false !== ($f = readdir($d))) {
			if(!is_dir($this->path.$f)) {
				$size = getimagesize($this->path.$f);
				$this->objTpl->setFile(1,'album-elem.tpl');
				$this->objTpl->setVars(1,array('MODULE','ID','NUM','NAME', 'SRC','SIZE'));
				$this->objTpl->setValues(1,array($this->module,$this->albumID ,$i, $f, 'http://'.$_SERVER['SERVER_NAME'].'/content/'.$this->module.'/'.$this->albumID.'/', $size[0].' x '.$size[1]));
				$photos .= $this->objTpl->getResult(1);
				$this->objTpl->unsetFile(1);
	   			$i++;
			}
		}
	 	if(empty($photos))
			$photos = '<div class="sectionEpmty">�������� ���!</div>';
        return $photos;
	}
	  function drawMap($par_id) {
		$sql = $this->objDb->query("select `ID` from `$this->album_table` where `PAR_ID`='$par_id'");
   		for ($i=0;$i<$this->objDb->num_rows($sql);$i++) {
   			$this->setAlbumProperties($this->objDb->result($sql,$i));
   			$options .= "<option value='$this->albumID'>".$this->getParentTitle($this->par_id).$this->title."</option>";

   			if($this->children==1)
   				$options .= $this->drawMap($this->albumID);

   		}
   		return $options;
	}
    function delete($id) {
		$this->objDb->query("DELETE FROM `$this->album_table` WHERE `ID` = '$id' LIMIT 1");
		$this->objFileSystem->removedir($this->objConf->globals['root'].'/content/'.$this->module.'/'.$id.'/');
		$sql = $this->objDb->query("SELECT `ID` FROM `$this->album_table` WHERE `PAR_ID` = '$id'");
	    for ($i=0;$i<$this->objDb->num_rows($sql);$i++)  {
	    	 $this->delete($this->objDb->result($sql, 0));
	    }
	}
}

?>
