<?
class Data {
    function Data(){
    }
    // CALENDAR FUNCTIONS
	function isLeapYear($y) {
		return ($y%4==0 && $y%100!=0) || $y%400==0;
	}
	function getDayInMonth($month,$year) {
		if(strlen($month) == 1)
			$month = '0'.$month;
		$arr = Array("01"=>31,"02"=>(isLeapYear($year) ? 29:28),"03"=>31,"04"=>30,"05"=>31,"06"=>30,"07"=>31,"08"=>31,"09"=>30,"10"=>31,"11"=>30,"12"=>31);
		return $arr[$month];
	}
	function calcNextWeek ($date) {
		list($year,$month,$day) = explode('-',$date);
		$days =  getDayInMonth($month, $year);
		if(($day+6)>$days) {
			$day = ($day+6)-$days;
			if($month<12) $month++;
			else {
					$month = 01;
				 	$year++;
			}
		}
		else $day += 6;
		return   $year.'-'.((strlen($month)==1) ?  '0'.$month :  $month).'-'.((strlen($day)==1) ?  '0'.$day :  $day);

	}
	function calcPrevWeek ($date) {
		list($year,$month,$day) = explode('-',$date);
		     if(($day-6)<1) {
		   		if($month>1) {
		   			$month--;
		   		}
		   		else {
		   			$month = 12;
		   			$year--;
		   		}
		        $days =  getDayInMonth($month, $year);
		        $day = $day-6+$days;
			 }
			 else {
			       $day -= 6;
			 }
			 return   $year.'-'.((strlen($month)==1) ?  '0'.$month :  $month).'-'.((strlen($day)==1) ?  '0'.$day :  $day);
	}
	function calcPrevDay ($date) {
		list($year,$month,$day) = explode('-',$date);
			if(($day-1)<1) {
		   		if($month>1) {
		   			$month--;
		}
		else {
			$month = 12;
		 	$year--;
		}
		$days =  getDayInMonth($month, $year);
		$day = $day-1+$days;
		}
			 else {
			       $day -= 1;
		}
		return  $year.'-'.((strlen($month)==1) ?  '0'.$month :  $month).'-'.((strlen($day)==1) ?  '0'.$day :  $day);
	}
	function renderMonth($month) {
		switch($month) {
			case('01'):
				$month = '������';
				break;
			case('02'):
				$month = '�������';
				break;
			case('03'):
				$month = '�����';
				break;
			case('04'):
				$month = '������';
				break;
			case('05'):
				$month = '���';
				break;
			case('06'):
				$month = '����';
				break;
			case('07'):
				$month = '����';
				break;
			case('08'):
				$month = '�������';
				break;
			case('09'):
				$month = '��������';
				break;
			case('10'):
				$month = '�������';
				break;
			case('11'):
				$month = '������';
				break;
			case('12'):
				$month = '�������';
				break;
		}
		return $month;
	}
	function renderDate($date, $del) {		list($x,$y,$z) = explode($del,$date);
		return $x.' '.$this->renderMonth($y).' '.$z;	}
}
?>