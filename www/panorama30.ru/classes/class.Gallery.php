<?php
require_once $classes_dir.'class.Img.php';
class Gallery {
    function Gallery($module,$moduleMame){
        $this->objDb = new Database();
        $this->objTpl = new Templates();
        $this->objConf = new Config();
$this->objFileSystem = new FileSystem();
        $this->module = $module;
	 	$this->moduleName = $moduleMame;

        $this->album_table = $this->module.'_albums';
        $this->language = $this->objConf->setLanguage();
        $this->setCount();

        $this->albumID = (isset($this->objConf->arr['album'])) ? $this->objConf->arr['album'] : 0;


        $this->path = $this->objConf->globals['root'].'/content/'.$this->module.'/'.$this->albumID.'/';
        $this->http_path = $this->objConf->globals['http_root'].'content/'.$this->module.'/'.$this->albumID.'/';

	}
	function setCountAlbums(){
     	$sql = $this->objDb->query("select count(*) from `$this->item_table`");
     	$this->count = $this->objDb->result($sql,0);
    }
	function getParentTitle ($id)  {
		if($id==0)
			return '<a href="{HTTP_ROOT}'.$this->module.'">�������</a>->';
        $sql = $this->objDb->query("select * from `$this->album_table` where `ID`='$id'  limit 1");
    	if($this->objDb->num_rows($sql)) {
	    	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
	    	$path='<a href="{HTTP_ROOT}'.$this->module.'/album'.$row["ID"].'">'.$row["TITLE"].'</a>->';
	     	$path =  $this->getParentTitle($row["PAR_ID"]).$path;
	    	return $path;
    	}
	}
    function setAlbumProperties($id){
    	$sql = $this->objDb->query("select * from `$this->album_table` where `ID`='$id' and `LANG`='$this->language'  limit 1");
    	if($this->objDb->num_rows($sql)>0) {
	       	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
	       	$this->albumID = $id;
	       	$this->title = $row["TITLE"];
	       	$this->text = $row["TEXT"];
	       	$this->par_id = $row["PAR_ID"];
	        $this->date = $row["DATE"];
	        if($this->objDb->num_rows($this->objDb->query("SELECT * FROM `$this->album_table` WHERE `PAR_ID`='$this->albumID'"))>0)
       			$this->children = 1;
       		else
       			$this->children = 0;

	        return 1;
        }
        return 0;
    }

	function setCount(){
     	$sql = $this->objDb->query("select count(*) from `$this->album_table`  where `LANG`='$this->language'");
     	$this->count = $this->objDb->result($sql,0);
    }

    function drawThis(){
    	if($this->albumID) {
 			if(!$this->setAlbumProperties($this->albumID)) {
          		$this->error = '����� ������� �� ����������!';
          		return false;
          	}

 		    $photos = $this->drawPhotos($this->albumID);
            $albums = $this->drawAlbums($this->albumID);

			$this->objTpl->setFile(1,'album.tpl');
			$this->objTpl->setVars(1,array('MODULE', 'MODULE_NAME','ID','TITLE','DATE', 'TEXT', 'PHOTOS', 'ALBUMS', 'HIERARCHY'));
			$this->objTpl->setValues(1,array($this->module, $this->moduleName, $this->albumID,$this->title,$this->objDb->doutput($this->date),$this->text, $photos, $albums, $this->getParentTitle($this->par_id)));
		 	$this->renderArray['content'] = $this->objTpl->getResult(1);
			$this->renderArray['title'] = $this->title;
        }
        else {
	        $albums = $this->drawAlbums(0);
	 		$this->objTpl->setFile(1,'gallery.tpl');
	  		$this->objTpl->setVars(1,array('MODULE','ALBUMS', 'PAGE_TITLE'));
	        $this->objTpl->setValues(1,array($this->module,$albums, ''));
	        $this->renderArray['content'] = $this->objTpl->getResult(1);
		    $this->renderArray['title'] = $this->moduleName;
        }
        $this->objTpl->unsetFile(1);
        $this->renderArray['module_name'] = $this->moduleName;
    }
    function drawAlbums($par_id) {
 		$albums = '';
   		$sql = $this->objDb->query("select `ID` from `$this->album_table` WHERE `PAR_ID`='$par_id' AND `LANG` = '$this->language' order by `DATE` ");
	   	for ($i=0;$i<$this->objDb->num_rows($sql);$i++) {
	    	$albums .= $this->drawAlbumElement($this->objDb->result($sql,$i),$i+1);
	    	if(($i+1)%3==0) $albums .= '</tr><tr>';
	    }
	    if(empty($albums))
	    	$albums = '<div class="sectionEpmty"></div>';
	    return $albums;
	}
	function drawAlbumElement($id,$i){
		$this->path = $this->objConf->globals['root'].'/content/'.$this->module.'/'.$id.'/';
        $this->http_path = $this->objConf->globals['http_root'].'/content/'.$this->module.'/'.$id.'/';
		if(is_dir($this->path)) {
			$d = opendir($this->path);
		    while(false !== ($f = readdir($d))) {
				if(!is_dir($this->path.$f)) {
					$img = $this->http_path.'thumbs/'.$f;
					break;
				}
			}
		}
		if(!isset($img) || empty($img) )
			$img = $this->objConf->globals["http_root"].'content/default.jpg';

 		$sql = $this->objDb->query("select * from `$this->album_table` where `ID`='$id'  limit 1");
       	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
   		$this->objTpl->setFile(1,'gallery-elem.tpl');
  		$this->objTpl->setVars(1,array('MODULE','ID','IMG','TITLE','TEXT','DATE', 'NUM', 'PREV'));
    	$this->objTpl->setValues(1,array($this->module,$id,'',$row["TITLE"],$row["TEXT"],str_replace('-','.',$this->objDb->doutput($row["DATE"])), $i,$img));
    	$content = $this->objTpl->getResult(1);
        $this->objTpl->unsetFile(1);
        return $content;
	}
	function drawPhotos($id) {
	 	if(!is_dir($this->path))
			mkdir($this->path, 0644);
		$photos='';
	 	$d = opendir($this->path) or die("Couldn't open images directory");
		while(false !== ($f = readdir($d))) 
			if(!is_dir($this->path.$f)) $arr[]=$f;		
			
natsort($arr);
$i=1;
foreach($arr as $f) {

$photos .= '<td><a target="_new" href="'.$this->http_path.$f.'"><img width=150 class=f src="'.$this->http_path.'thumbs/'.$f.'" style="margin: 5px"></a></td>';
	   			if($i%3==0) $photos .= '</tr><tr>';
$i++;
}
	 	if(empty($photos))
			$photos = '<div>�������� ���!</div>';
        return $photos;
	}
	function getNameMount($m) {
	    switch($m)  {
	    case("01"):
	    	return '������';
	    case("02"):
	    	return '�������';
	    case("03"):
	    	return '����';
	    case("04"):
	    	return '������';
	    case("05"):
	    	return '���';
	    case("05"):
	    	return '����';
	    case("06"):
	    	return '����';
	    case("07"):
	    	return '������';
	    case("08"):
	    	return '��������';
	    case("09"):
	    	return '�������';
	    case("10"):
	   	 	return '������';
	    case("11"):
	    	return '�������';
	    }
	}
}
?>
