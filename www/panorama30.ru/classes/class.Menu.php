<?php
class Menu {
    function Menu($module, $separator = null){
    	$this->module = ($module) ?  $module : 0;
        $this->objDb 	= new Database();
        $this->objConf 	= new Config();
        $this->objTpl 	= new Templates();
        $this->objMod	= new Modules();
       	$this->separator = $separator;
	 	$this->table = $this->module;
	 	$this->language = $this->objConf->setLanguage();
	 	$this->mainID =  1;
        if(empty($this->objConf->query[0]) and $this->module=='menu') {
		     $this->objConf->query = $this->getDefaultPage();
		     $this->main =  true;
		}

		$this->page_current = $this->objMod->getModuleByPath($this->objConf->query[0]);

		if($this->page_current) {
			require_once $this->objConf->globals['root'].'/classes/class.'.$this->page_current["MODULE"].'.php';
			eval('$this->objPrt = '."new ".$this->page_current["MODULE"]." ('".$this->page_current["PATH"]."','".$this->page_current["TITLE"]."');");
			if($this->page_current['MODULE'] == 'StaticPage') {
		         $this->objPrt->getPageId($this->objConf->query);
				 $this->page_current = $this->page_current['ID'].'_'.$this->objPrt->drawPageID;
		   	}
		   	else
		         $this->page_current =  $this->page_current['ID'];
		    $this->page_current = $this->getElementByMId($this->page_current);
		    $this->page_current = $this->page_current['POS'];
		    $this->main = ($this->page_current == $this->mainID) ? true: false;
     	}
     	else {
     		$this->objPrt = $this;
     		$this->error = '�������� �������� ������';
     	}
	}
	function setProperties($id){
    	$sql = $this->objDb->query("select * from `$this->table` where `ID`='$id' and `LANG`='$this->language'  limit 1");
    	if($this->objDb->num_rows($sql)>0) {
	       	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));

	       	$this->m_id =  $row["M_ID"];
	    	$arr = explode('_', $this->m_id);
      		$this->m_id = ($arr[0])? $arr[0] : $this->m_id;

	       	$row_mod = $this->objMod->getModuleById($this->m_id);
            $this->m_path = $row_mod['PATH'];
            //======================================================================
	        // ���� ��������� ��������
	        //======================================================================
	       	if(isset($arr[1])) {
	   			require_once $this->objConf->globals['root'].'/classes/class.'.$row_mod['MODULE'].'.php';
				eval('$this->objPrt = '."new ".$row_mod['MODULE']." ('".$row_mod['PATH']."','".$row_mod['TITLE']."');");
	       	    $row_page = $this->objPrt->getPageById($arr[1]);
                $this->p_path = $row_page['PATH'];
                $this->p_title = $row_page['TITLE'];
	       	}
	       	//======================================================================
	        // ��� ���������
	        //======================================================================
	       	else {
	       		$this->p_path = '';
	       		$this->p_title = $row_mod['TITLE'];
	       	}
	       	$this->pos = $row['POS'];
	        return 1;
        }
        return 0;
    }
   	function drawThis() {
   		$this->renderedContent ='';
		$sql = $this->objDb->query("select * from `$this->table` WHERE `LANG`='$this->language' ORDER BY `POS` ASC");
	    for ($i=0;$i<$this->objDb->num_rows($sql);$i++)
	    	$this->renderedContent .= $this->drawMenuElement($this->objDb->result($sql,$i),$i+1);
		$this->objTpl->setFile(1,$this->module.'.tpl');
		$this->objTpl->setVars(1,array('MODULE','ELEMENTS'));
		$this->objTpl->setValues(1,array($this->module ,$this->renderedContent));
		$this->renderedContent = $this->objTpl->getResult(1);
	 	$this->objTpl->unsetFile(1);
    }
    function drawMenuElement($id, $num) {
    	$element = '';
    	$this->setProperties($id);
        if($num!=1 && $this->separator)
        	$element .=  $this->separator;
    	if($this->page_current == $this->pos) {
    		$this->objTpl->setFile(1,$this->module.'-elem-current.tpl');
    	}
    	else
     		$this->objTpl->setFile(1,$this->module.'-elem.tpl');
	  	$this->objTpl->setVars(1,array('MODULE','ID','MODULE_PATH','PAGE_PATH','PAGE_TITLE', 'POS' ,'NUM'));
	   	$this->objTpl->setValues(1,array($this->module, $this->id,$this->m_path,$this->p_path, $this->p_title,$this->pos, $num));
	   	$element .= $this->objTpl->getResult(1);
	   	$this->objTpl->unsetFile(1);
	   	return $element;
    }
    function getElementByMId($id) {
    	$sql = $this->objDb->query("select * from `$this->table` where `M_ID`='$id' and `LANG`='$this->language'  limit 1");
        if($this->objDb->num_rows($sql)>0) {
	       	$row = $this->objDb->aoutput($this->objDb->fetch_array($sql));
	       	return $row;
    	}
    	return 0;
    }
    function getDefaultPage() {
    	$sql = $this->objDb->query("select `ID` from `$this->table` WHERE `LANG`='$this->language' AND `POS`='$this->mainID' LIMIT 1");
        $this->setProperties($this->objDb->result($sql,0));
        return array($this->m_path,$this->p_path);
    }
}
?>
