<div id="rating_vote">
<div class="title">Рейтинг!</div>
<p id="vote"></p>
<div class="rate_num">{RATING}</div>
<div class="num_text">{NUM_TEXT}</div>
</div>
<script>
var activeImageSrc = "{HTTP_ROOT}design/active_star.gif";
var passiveImageSrc = "{HTTP_ROOT}design/passive_star.gif";
var loader= '<img src="{HTTP_ROOT}design/loadingAnimation.gif" width="150px" />';
var maxScore = 10;
var messages = Array('','Проголосовать - <b>1 балл</b>','Проголосовать - <b>2 балла</b>','Проголосовать - <b>3 балла</b>','Проголосовать - <b>4 балла</b>','Проголосовать - <b>5 баллов</b>','Проголосовать - <b>6 баллов</b>','Проголосовать - <b>7 баллов</b>','Проголосовать - <b>8 баллов</b>','Проголосовать - <b>9 баллов</b>','Проголосовать - <b>10 баллов</b>');
$(document).ready(function(){
    $("#vote").each(function(){    	var $container = $(this);
    	for (var i = 0, num = maxScore; i < num; ++i) {
	   		$("<img />").appendTo($container);
		}
    	$("<div class=rateHelp />").appendTo($container);
		$container.find("img").attr("src", passiveImageSrc).css({display: "inline", cursor: 'pointer'})
		.bind("mouseover", function(e) {
			var len = $container.find("img").index(e.target) + 1;
			$container.find("img").slice(0, len).attr("src", activeImageSrc);
			$container.find("div").html(messages[len]);
		})
		.bind("mouseout", function(e) {
			var len = $container.find("img").index(e.target) + 1;
			$container.find("img").slice(0, len).attr("src", passiveImageSrc);
			$container.find("div").html("");
		}).
		bind("click", function(e) {
			var len = $container.find("img").index(e.target) + 1;
			$container.find("img").unbind("mouseover").unbind("mouseout").unbind("click").css({opacity: 0.3});
			$container.find("div").html(loader);
			$.post('{HTTP_ROOT}system/rating.php',{id: {ID}, sum: len, act: 'vote'}, renderVote);
		});
		function renderVote(data){
		   $('#rating_vote').replaceWith(data);
		}
	});
});
</script>