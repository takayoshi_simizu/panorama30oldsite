<div id="votes">
	<div class=title>{MODULE_NAME}</div>
	{VOTES}
</div>
<script>
$(document).ready(function(){
    $('div.vote').each(function(){    	var obj = this;
    	$(this).children(' :radio').click(function(){
			check(obj);
    	});
    	check(obj);    }); 	$('img.submitVote').click(function(){
 		if($(this).attr('mode')=='true') {
		 	var _img = this;
		 	$(this).css({cursor: 'default', opacity: 0.4}).attr('mode','false');

		 	var id = this.id.substring(6, this.id.length);
		 	var a_id = $('div :checked').attr('value');

		 	$.post('{HTTP_ROOT}system/vote.php',{id: id, a_id: a_id, act: 'vote'}, renderVote);
        }
        function renderVote(data){
		  	$(_img).css({cursor: 'pointer', opacity: 1}).attr('mode','true');
		    $('#vote'+id).replaceWith(data);
		}
 	})
})
function check(obj) {	var id = obj.id.substring(4, obj.id.length);
	var selected = $('div :checked').attr('value');
	if(selected != undefined) {		$('img#submit'+id).css({cursor: 'pointer', opacity: 1}).attr('mode','true');
	}
	else {
		$('img#submit'+id).css({cursor: 'default', opacity: 0.4}).attr('mode','false');
	}
}
</script>